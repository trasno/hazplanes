<?php
include_once("../includes/config.inc.php");
include_once("../includes/general.inc.php");
require_once("../includes/api.inc.php");

$status = "ko";
$msg = "";

if(isset($_POST["type"]) && !empty($_POST["type"]) && isset($_POST["search"])) {
    $type = filter_input(INPUT_POST, 'type', FILTER_SANITIZE_STRING);
    $search = filter_input(INPUT_POST, 'search', FILTER_SANITIZE_STRING);
    //if(empty($search)) {
        $options["limit"] = 50;
        $options["start"] = 0;
    //}
    switch($type) {
        case "peliculas":
            $options["filter"] = "nombre like '%".$search."%' or original like '%".$search."%' or fechaestreno like '%".$search."%'";
            $eventos = new Peliculas($options,$db);
            $link = "pelicula/";
            $adminlink = "adminformfilm";
            $id = "idpelicula";
            $genero = "genero";
            break;
        case "conciertos":
            $options["filter"] = "c.nombre like '%".$search."%' or lc.nombre like '%".$search."%'";
            $eventos = new Conciertos($options,$db);
            $link = "concierto/";
            $adminlink = "adminformconcert";
            $id = "idconcierto";
            $genero = "genero";
            break;
        case "obrasteatro":
            $options["filter"] = "o.nombre like '%".$search."%' or lo.nombre like '%".$search."%'";
            $eventos = new Obras($options,$db);
            $link = "obra/";
            $adminlink = "adminformobra";
            $id = "idobrateatro";
            $genero = "genero";
            break;
        case "eventos":
            $options["filter"] = "e.nombre like '%".$search."%' or le.nombre like '%".$search."%'";
            $eventos = new Eventos($options,$db);
            $link = "evento/";
            $adminlink = "adminformevent";
            $id = "ideventos";
            $genero = "tipo";
            break;
        case "cursos":
            $options["filter"] = "c.nombre like '%".$search."%' or lc.nombre like '%".$search."%' or fecha like '%".$search."%'";
            $eventos = new Cursos($options,$db);
            $link = "curso/";
            $adminlink = "adminformcurso";
            $id = "idcurso";
            $genero = "tipo";
            break;
        case "deportes":
            $options["filter"] = "d.nombre like '%".$search."%' or ld.nombre like '%".$search."%' or fecha like '%".$search."%'";
            $eventos = new Deportes($options,$db);
            $link = "competicion/";
            $adminlink = "adminformdeporte";
            $id = "iddeporte";
            if(!empty($d["tiponombre"])) {
                $genero = "tiponombre";
            } else {
                $genero = "genero";
            }
            break;
        case "exposiciones":
            $options["filter"] = "e.nombre like '%".$search."%' or me.nombre like '%".$search."%' or fin like '%".$search."%'";
            $eventos = new Exposiciones($options,$db);
            $link = "exposicion/";
            $adminlink = "adminformexpo";
            $id = "idexposicion";
            $genero = "genero";
            break;
        case "cines":
            $options["filter"] = "nombre like '%".$search."%' or direccion like '%".$search."%' or localidad like '%".$search."%' or tipolocal like '%".$search."%'";
            $eventos = new Cines($options,$db);
            $link = "cine/";
            $adminlink = "adminformcine";
            $id = "idcine";
            $genero = "local";
            break;
        case "locales":
            $options["filter"] = "nombre like '%".$search."%' or direccion like '%".$search."%' or localidad like '%".$search."%' or tipolocal like '%".$search."%'";
            $eventos = new Locales($options,$db);
            $link = "local/";
            $adminlink = "adminformlocal";
            $id = "idlocal";
            $genero = "local";
            break;
        case "teatros":
            $options["filter"] = "nombre like '%".$search."%' or direccion like '%".$search."%' or localidad like '%".$search."%' or tipolocal like '%".$search."%'";
            $eventos = new Teatros($options,$db);
            $link = "auditorio/";
            $adminlink = "adminformtheater";
            $id = "idteatro";
            $genero = "local";
            break;
        case "museos":
            $options["filter"] = "nombre like '%".$search."%' or direccion like '%".$search."%' or localidad like '%".$search."%' or tipolocal like '%".$search."%'";
            $eventos = new Museos($options,$db);
            $link = "museo/";
            $adminlink = "adminformmuseo";
            $id = "idmuseo";
            $genero = "local";
            break;
        case "pabellones":
            $options["filter"] = "nombre like '%".$search."%' or direccion like '%".$search."%' or localidad like '%".$search."%' or tipolocal like '%".$search."%'";
            $eventos = new Pabellones($options,$db);
            $link = "pabellon/";
            $adminlink = "adminformpabellon";
            $id = "idpabellon";
            $genero = "local";
            break;
    }
    $data = $eventos->readAction();

    $msg = array();
    foreach($data as $k => $d) {
        $enlace = BASE_URL.$link.$d[$id]."-".urlAmigable($d["nombre"]);
        if($genero != "local") {
        	$original = "";
			if($type == "peliculas") {
				$original = " (".$d["original"].")";
			}
            $row = "<tr class='row'>
                <td><input type='checkbox' name='checkListItem' id='".$d[$id]."' class='' title='Seleccionar/Deseleccionar'/></td>
                <td>".($d["publicado"]?"Si":"No")."</td>
                <td><a href='index.php?p=".$adminlink."&i=".$d[$id]."'>".$d["nombre"].$original."</a></td>";
            if($type != "peliculas") {
                $row .= "<td>".$d["lugar"]."</td>";
            }
            $row .= "<td>".($d["infantil"]?"Infantil - ":"").$d[$genero]."</td>";
            if($type != "peliculas") {
                if($type == "exposiciones") {
                    $row .= "<td>".date("d-m-Y",strtotime($d["fin"]))."</td>";
                } else {
                    $row .= "<td>".date("H:i d-m-Y",strtotime($d["fecha"]))."</td>";
                }
            } else {
                $row .= "<td>".(empty($d["fechaestreno"])?"":date("d-m-Y",strtotime($d["fechaestreno"])))."</td>";
            }
            $row .= "<td>".(empty($d["poster"])?"No":"Si")."</td>
                <td><a href='".$enlace."'>".$enlace."</a></td>
            </tr>";
        } else {
            $row = "<tr class='row'>
                        <td><input type='checkbox' name='checkListItem' id='".$d[$id]."' class='' title='Seleccionar/Deseleccionar'/></td>
                        <td class='listPublishedCell'>".($d["publicado"]?"Si":"No")."</td>
                        <td><a href='index.php?p=".$adminlink."&i=".$d[$id]."'>".$d["nombre"]."</a></td>
                        <td>".$d["direccion"]."</td>
                        <td>".$d["localidad"]."</td>
                        <td>".$d["tipolocal"]."</td>
                        <td>".(empty($d["imagen"])?"No":"Si")."</td>
                        <td><a href='".$enlace."'>".$enlace."</a></td>
                    </tr>";
        }
        $msg[] = $row;
    }
    $status = "ok";
}

$result = array(
    "status" => $status,
    "msg" => $msg
    );

echo json_encode($result);

?>