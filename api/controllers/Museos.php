<?php
/**
 * Clase controlador de museos
 *
 * @package API
 * @author Trasno
 */
class Museos {
    private $params;
    private $general;
    private $museoItem;
    //private $serviciosItem;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct($params, Db $db) {
        $this->params = $params;
        $this->general = new General($db);
        $this->museoItem = new MuseoItem($db);
        //$this->serviciosItem = new ServiciosItem($db);
    }

    /**
     * Destructor
     *
     * @return void
     * @author trasno
     */
    public function __destruct() {
        $this->museoItem = null;
    }

    /**
     * Crear museo
     *
     * @return int
     */
    public function createAction() {
        $museo = array();
        foreach($this->params as $key => $value){
            $museo[$key] = $value;
        }

        //Cogemos los datos de los servicios para utilizarlos una vez creado el museo
        /*if(isset($museo["servicios"])) {
            $servicios = $museo["servicios"];
            foreach($servicios as $k => $s) {
                $servicios[$k] = 1;
            }
        } else {
            $servicios = array();
        }*/

        //Cogemos los datos de las promociones para utilizarlos una vez creado el museo
        /*if(isset($museo["promos"])) {
            $promos = $museo["promos"];
        } else {
            $promos = array();
        }*/

        //Cogemos los datos de los horario para utilizarlos una vez creado el museo
        if(isset($museo["horario"])) {
            $horario = $museo["horario"];
        } else {
            $horario = array();
        }

        unset($museo["servicios"],$museo["promos"],$museo["horario"]);
        $museo["publicado"] = array_key_exists("publicado",$museo)? 1:0;

        //Creamos el museo
        $result = $this->museoItem->addMuseo($museo);

        if($result) {
            //Añadir servicios al museo
            //$servicios["idmuseo"] = $result;
            //$this->serviciosItem->addServicios($servicios);
            //Añadir promociones
            //foreach($promos as $k => $promo) {
            //    $this->museoItem->setPromociones($result, $promo);
            //}

            //Añadir horario
            $horario["idlugar"] = $result;
            $horario["tipolugar"] = "museo";
            $this->general->addHorario($horario);
        }

        return $result;
    }

    /**
     * Recuperar museo(s)
     *
     * @return array
     */
    public function readAction() {
        if(isset($this->params["idmuseo"]) && !empty($this->params["idmuseo"])) {
            $data = $this->museoItem->dataMuseo($this->params["idmuseo"]);
        } else {
            $options = array(
                "limit" => 0,
                "start" => 0,
                "filter" => "",
                "order" => "nombre"
                );
            if(!empty($this->params)) {
                $options = array_merge($options,$this->params);
            }
            $data = $this->museoItem->listMuseos($options);
        }

        return $data;
    }

    /**
     * Actualizar museo
     *
     * @return boolean
     */
    public function updateAction() {
        if(isset($this->params["op"])) {
            if(strtolower($this->params["op"]) == "publish") {
                $result = $this->museoItem->publishMuseo($this->params["ids"]);
            } elseif(strtolower($this->params["op"]) == "unpublish") {
                $result = $this->museoItem->unpublishMuseo($this->params["ids"]);
            } else {
                $result = false;
            }
        } else {
            $museo = array();
            foreach($this->params as $key => $value){
                $museo[$key] = $value;
            }

            //Servicios
            /*if(isset($museo["servicios"])) {
                $servicios = $museo["servicios"];
                foreach($servicios as $k => $s) {
                    $servicios[$k] = 1;
                }
            } else {
                $servicios = array();
            }
            $servicios["idmuseo"] = $museo["idmuseo"];
            $this->serviciosItem->updateServicios($servicios);*/

            //Promociones
            /*$bdRelMuseoPromo = $this->museoItem->getPromociones($museo["idmuseo"]);
            if(isset($museo["promos"])) {
                $promos = $museo["promos"];
            } else {
                $promos = array();
            }
            foreach($promos as $k => $promo) {
                $existe = false;
                foreach($bdRelMuseoPromo as $key => $value) {
                    if($value["idpromocion"] == $promo) {
                        $existe = true;
                        unset($bdRelMuseoPromo[$key]);
                        break;
                    }
                }
                if(!$existe) {
                    $this->museoItem->setPromociones($museo["idmuseo"], $promo);
                }
            }
            foreach($bdRelMuseoPromo as $s) {
                $this->museoItem->delPromociones($museo["idmuseo"],$s["idpromocion"]);
            }*/

            //Horario
            $options["idlugar"] = $museo["idmuseo"];
            $options["tipolugar"] = "museo";
            $bdRelMuseoHorario = $this->general->listHorarios($options);
            if(isset($museo["horario"])) {
                $horario = $museo["horario"];
            } else {
                $horario = array();
            }
            $horario["idlugar"] = $museo["idmuseo"];
            $horario["tipolugar"] = "museo";
            if(count($bdRelMuseoHorario) > 0) {
                $this->general->updateHorario($horario);
            } else {
                $this->general->addHorario($horario);
            }

            unset($museo["servicios"],$museo["promos"],$museo["horario"],$bdRelMuseoPromo,$promos);
            $museo["publicado"] = array_key_exists("publicado",$museo)? 1:0;

            //Museo
            $result = $this->museoItem->updateMuseo($museo);
        }

        return $result;
    }

    /**
     * Eliminar museo
     *
     * @return boolean
     *
     * TODO: eliminar conciertos/obras del museo a eliminar
     */
    public function deleteAction() {
        if(isset($this->params["image"])) {
            $result = $this->general->deleteImage($this->params["id"], $this->params["image"]);
        } else {
            //$result = $this->serviciosItem->deleteServicios($this->params["idmuseo"]);
            //$result = $this->museoItem->delPromociones($this->params["idmuseo"]);
            $options["idlugar"] = $this->params["id"];
            $options["tipolugar"] = "museo";
            $result = $this->general->deleteHorario($options);
            if($result) {
                $result = $this->museoItem->deleteMuseo($this->params["id"]);
            }
        }

        return $result;
    }
}
