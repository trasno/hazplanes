<?php
/**
 * Clase controlador de deportes
 *
 * @package API
 * @author Trasno
 */
class Deportes {
    private $params;
    private $general;
    private $deportesItem;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct($params, Db $db) {
        $this->params = $params;
        $this->general = new General($db);
        $this->deportesItem = new DeporteItem($db);
    }

    /**
     * Destructor
     *
     * @return void
     * @author trasno
     */
    public function __destruct() {
        $this->deportesItem = null;
    }

    /**
     * Crear deportes
     *
     * @return int
     */
    public function createAction() {
        if(isset($this->params["tipos"]) && $this->params["tipos"] == "create") {
            $tipodeporte = array();
            foreach($this->params as $key => $value){
                $tipodeporte[$key] = $value;
            }
            unset($tipodeporte["tipos"]);
            $result = $this->deportesItem->addTipoDeporte($tipodeporte);
        } else {
            $deportes = array();
            foreach($this->params as $key => $value){
                $deportes[$key] = $value;
            }

            $horarios = array();
            $tipohorarios = array("locales","auditorios","cines","lugar","museos","pabellones");
            foreach($tipohorarios as $t) {
                if(isset($deporte[$t])) {
                    $horarios[$t] = $deportes[$t];
                }
                unset($deportes[$t]);
            }

            $deportes["publicado"] = array_key_exists("publicado",$deportes)? 1:0;
            $deportes["infantil"] = array_key_exists("infantil",$deportes)? 1:0;
            $deportes["reserva"] = array_key_exists("reserva",$deportes)? 1:0;

            $result = $this->deportesItem->addDeportes($deportes);

            if($result) {
                //Insertar los horarios
                foreach($horarios as $tipo => $horario) {
                    if(isset($horario[0][0]["fecha"])) {
                        switch($tipo) {
                            case "locales":
                                $idtipo = "idlocal";
                                $addtipo = "local";
                                break;
                            case "auditorios":
                                $idtipo = "idteatro";
                                $addtipo = "teatro";
                                break;
                            case "cines":
                                $idtipo = "idcine";
                                $addtipo = "cine";
                                break;
                            case "lugar":
                                $idtipo = "";
                                $addtipo = "lugar";
                                break;
                            case "museos":
                                $idtipo = "idmuseo";
                                $addtipo = "museo";
                                break;
                            case "pabellones":
                                $idtipo = "idpabellon";
                                $addtipo = "pabellon";
                                break;
                        }
                        foreach($horario as $key => $local) {
                            foreach($local as $k => $hora) {
                                if(isset($local[0][$idtipo])) {
                                    $id = $local[0][$idtipo];
                                } else {
                                    $id = "";
                                }
                                $pase = array(
                                    "idlugar" => $id,
                                    "iddeporte" => $result,
                                    "fecha" => $hora["fecha"],
                                    "precio" => $hora["precio"],
                                    "agotado" => array_key_exists("agotado",$hora)? 1:0,
                                    "cancelado" => array_key_exists("cancelado",$hora)? 1:0,
                                    "todoeldia" => array_key_exists("tododia",$hora)? 1:0
                                    );
                                if($tipo == "cines") {
                                    $pase["idsala"] = $hora["idsala"];
                                    $pase["3d"] = array_key_exists("3d",$hora)? 1:0;
                                    $pase["vo"] = array_key_exists("vo",$hora)? 1:0;
                                } else {
                                    $pase["precioanticipada"] = $hora["precioanticipada"];
                                }
                                if($tipo == "lugar") {
                                    $pase["coordenadas"] = trim($hora["coordenadas"]);
                                    $pase["lugar"] = trim($hora["lugar"]);
                                    $pase["localidad"] = trim($hora["localidad"]);
                                } else {
                                    $pase["urlcompra"] = trim($hora["urlcompra"]);
                                }
                                $this->deportesItem->addHora($pase,$addtipo);
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Recuperar deporte(s)
     *
     * @return array
     */
    public function readAction() {
        if(isset($this->params["agenda"])) {
            $options = array(
                "limit" => 0,
                "start" => 0,
                "filter" => "fecha >= NOW()",
                "order" => "fecha, d.nombre",
                );
            $options = array_merge($options,$this->params);
            $data = $this->deportesItem->listDeportes($options);
        } else {
            if(isset($this->params["read"])) {
                if($this->params["read"] == "tiposdeporte") {
                    $options = array(
                        "limit" => 0,
                        "start" => 0,
                        "filter" => "",
                        "order" => "nombre"
                        );
                    $data = $this->deportesItem->listTiposDeporte($options);
                } elseif($this->params["read"] == "datatipodeporte") {
                    $data = $this->deportesItem->dataTipoDeporte($this->params["idtipodeporte"]);
                }
            } else {
                if(isset($this->params["iddeporte"]) && !empty($this->params["iddeporte"])) {
                    $data = $this->deportesItem->dataDeportes($this->params["iddeporte"]);
                    $horas = $this->deportesItem->listHoras($data[0]);
                    $data[0]["horasPabellon"] = $horas["horasPabellon"];
                    $data[0]["horasLocal"] = $horas["horasLocal"];
                    $data[0]["horasAuditorio"] = $horas["horasAuditorio"];
                    $data[0]["horasCine"] = $horas["horasCine"];
                    $data[0]["horasLugar"] = $horas["horasLugar"];
                    $data[0]["horasMuseo"] = $horas["horasMuseo"];
                } else {
                    $options = array(
                        "limit" => 0,
                        "start" => 0,
                        "filter" => "",
                        "order" => "iddeporte desc"
                        );
                    if(!empty($this->params)) {
                        $options = array_merge($options,$this->params);
                    }
                    $data = $this->deportesItem->listDeportes($options);
                }
            }
        }
        if(empty($data)) {
            $data = array();
        }

        return $data;
    }

    /**
     * Actualizar deportes
     *
     * @return boolean
     */
    public function updateAction() {
        if(isset($this->params["op"])) {
            if(strtolower($this->params["op"]) == "publish") {
                $result = $this->deportesItem->publishDeportes($this->params["ids"]);
            } elseif(strtolower($this->params["op"]) == "unpublish") {
                $result = $this->deportesItem->unpublishDeportes($this->params["ids"]);
            } elseif(strtolower($this->params["op"]) == "field") {
                $fields = array();
                foreach($this->params as $key => $value){
                    $fields[$key] = $value;
                }
                unset($fields["op"]);
                $result = $this->deportesItem->updateDeportes($fields);
            } else {
                $result = false;
            }
        } elseif(isset($this->params["tipos"]) && $this->params["tipos"] == "update") {
            $tipodeporte = array();
            foreach($this->params as $key => $value){
                $tipodeporte[$key] = $value;
            }
            unset($tipodeporte["tipos"]);
            $result = $this->deportesItem->updateTipoDeporte($tipodeporte);
        } else {
            $deportes = array();
            foreach($this->params as $key => $value){
                $deportes[$key] = $value;
            }

            //Horarios
            $bdhorarios = $this->deportesItem->listHoras(array("iddeporte" => $deportes["iddeporte"]));
            $tipohorarios = array("horasLocal", "horasAuditorio", "horasCine", "horasLugar", "horasMuseo", "horasPabellon");
            foreach($tipohorarios as $th) {
                $bdhoras = $bdhorarios[$th];
                switch($th) {
                    case "horasLocal":
                        $formfield = "locales";
                        $idtipo = "idlocal";
                        $addtipo = "local";
                        break;
                    case "horasAuditorio":
                        $formfield = "auditorios";
                        $idtipo = "idteatro";
                        $addtipo = "teatro";
                        break;
                    case "horasCine":
                        $formfield = "cines";
                        $idtipo = "idcine";
                        $addtipo = "cine";
                        break;
                    case "horasLugar":
                        $formfield = "lugar";
                        $idtipo = "";
                        $addtipo = "lugar";
                        break;
                    case "horasMuseo":
                        $formfield = "museos";
                        $idtipo = "idmuseo";
                        $addtipo = "museo";
                        break;
                    case "horasPabellon":
                        $formfield = "pabellones";
                        $idtipo = "idpabellon";
                        $addtipo = "pabellon";
                        break;
                }
                if(isset($deportes[$formfield])) {
                    $horarios = $deportes[$formfield];
                } else {
                    $horarios = array();
                }

                foreach($horarios as $key => $local) {
                    foreach($local as $k => $hora) {
                        if(isset($local[0][$idtipo])) {
                            $id = $local[0][$idtipo];
                        } else {
                            $id = "";
                        }
                        if(isset($hora["fecha"])) {
                            $pase = array(
                                "idlugar" => $id,
                                "iddeporte" => $deportes["iddeporte"],
                                "fecha" => date("Y-m-d H:i",strtotime($hora["fecha"])),
                                "precio" => $hora["precio"],
                                "agotado" => array_key_exists("agotado",$hora)? 1:0,
                                "cancelado" => array_key_exists("cancelado",$hora)? 1:0,
                                "todoeldia" => array_key_exists("tododia",$hora)? 1:0
                                );
                            if($addtipo == "cine") {
                                $pase["idsala"] = $hora["idsala"];
                                $pase["3d"] = array_key_exists("3d",$hora)? 1:0;
                                $pase["vo"] = array_key_exists("vo",$hora)? 1:0;
                            } else {
                                $pase["precioanticipada"] = $hora["precioanticipada"];
                            }
                            if($addtipo == "lugar") {
                                $pase["coordenadas"] = trim($hora["coordenadas"]);
                                $pase["lugar"] = trim($hora["lugar"]);
                                $pase["localidad"] = trim($hora["localidad"]);
                            } else {
                                $pase["urlcompra"] = trim($hora["urlcompra"]);
                            }
                            $existe = false;
                            //Comprobamos si existe en la BD para modificarlo, en vez de darlo de alta de nuevo
                            foreach($bdhoras as $ke => $val) {
                                if(isset($val[$idtipo])) {
                                    if($val[$idtipo] == $pase["idlugar"] && $val["iddeporte"] == $pase["iddeporte"] && date("Y-m-d H:i",strtotime($val["fecha"])) == $pase["fecha"]) {
                                        $existe = true;
                                        unset($bdhoras[$ke]);
                                        break;
                                    }
                                } else {
                                    if($val["iddeporte"] == $pase["iddeporte"] && date("Y-m-d H:i",strtotime($val["fecha"])) == date("Y-m-d H:i",strtotime($pase["fecha"]))) {
                                        $existe = true;
                                        unset($bdhoras[$ke]);
                                        break;
                                    }
                                }
                            }
                            if($existe) {
                                $this->deportesItem->updateHora($pase,$addtipo);
                            } else {
                                $this->deportesItem->addHora($pase,$addtipo);
                            }
                        }
                    }
                }
                foreach($bdhoras as $delHora) {
                    if(isset($delHora[$idtipo])) {
                        $delHora["idlugar"] = $delHora[$idtipo];
                    }
                    $this->deportesItem->deleteHora($delHora,$addtipo);
                }
                unset($deportes[$formfield]);
            }

            $deportes["publicado"] = array_key_exists("publicado",$deportes)? 1:0;
            $deportes["infantil"] = array_key_exists("infantil",$deportes)? 1:0;
            $deportes["reserva"] = array_key_exists("reserva",$deportes)? 1:0;

            $result = $this->deportesItem->updateDeportes($deportes);
        }

        return $result;
    }

    /**
     * Eliminar deportes
     *
     * @return boolean
     */
    public function deleteAction() {
        if(isset($this->params["image"])) {
            $result = $this->general->deleteImage($this->params["id"], $this->params["image"]);
        } else {
            $result = $this->deportesItem->deleteDeportes($this->params["id"]);
        }

        return $result;
    }
}