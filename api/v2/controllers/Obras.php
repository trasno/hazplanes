<?php
/**
 * Clase controlador de obras
 *
 * @package API
 * @author Trasno
 */
class Obras {
    private $params;
    private $general;
    private $obraItem;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct($params, Db $db) {
        $this->params = $params;
        $this->general = new General($db);
        $this->obraItem = new ObraItem($db);
    }

    /**
     * Destructor
     *
     * @return void
     * @author trasno
     */
    public function __destruct() {
        $this->obraItem = null;
    }

    /**
     * Crear obra
     *
     * @return int
     */
    public function createAction() {
        $obra = array();
        foreach($this->params as $key => $value){
            $obra[$key] = $value;
        }

        $horarios = array();
        $tipohorarios = array("locales","auditorios","cines","lugar","museos");
        foreach($tipohorarios as $t) {
            if(isset($obra[$t])) {
                $horarios[$t] = $obra[$t];
            }
            unset($obra[$t]);
        }

        $obra["publicado"] = array_key_exists("publicado",$obra)? 1:0;
        $obra["infantil"] = array_key_exists("infantil",$obra)? 1:0;
        $obra["reserva"] = array_key_exists("reserva",$obra)? 1:0;

        $result = $this->obraItem->addObra($obra);

        if($result) {
            //Insertar los horarios
            foreach($horarios as $tipo => $horario) {
                if(isset($horario[0][0]["fecha"])) {
                    switch($tipo) {
                        case "locales":
                            $idtipo = "idlocal";
                            $addtipo = "local";
                            break;
                        case "auditorios":
                            $idtipo = "idteatro";
                            $addtipo = "teatro";
                            break;
                        case "cines":
                            $idtipo = "idcine";
                            $addtipo = "cine";
                            break;
                        case "lugar":
                            $idtipo = "";
                            $addtipo = "lugar";
                            break;
                        case "museos":
                            $idtipo = "idmuseo";
                            $addtipo = "museo";
                            break;
                    }
                    foreach($horario as $key => $local) {
                        foreach($local as $k => $hora) {
                            if(isset($local[0][$idtipo])) {
                                $id = $local[0][$idtipo];
                            } else {
                                $id = "";
                            }
                            $pase = array(
                                "idlugar" => $id,
                                "idobrateatro" => $result,
                                "fecha" => $hora["fecha"],
                                "precio" => $hora["precio"],
                                "agotado" => array_key_exists("agotado",$hora)? 1:0,
                                "cancelado" => array_key_exists("cancelado",$hora)? 1:0,
                                "todoeldia" => array_key_exists("tododia",$hora)? 1:0
                                );
                            if($tipo == "cines") {
                                $pase["idsala"] = $hora["idsala"];
                                $pase["3d"] = array_key_exists("3d",$hora)? 1:0;
                                $pase["vo"] = array_key_exists("vo",$hora)? 1:0;
                            } else {
                                $pase["precioanticipada"] = $hora["precioanticipada"];
                            }
                            if($tipo == "lugar") {
                                $pase["coordenadas"] = trim($horario[0][0]["coordenadas"]);
                                $pase["lugar"] = trim($hora["lugar"]);
                                $pase["localidad"] = trim($hora["localidad"]);
                            } else {
                                $pase["urlcompra"] = trim($hora["urlcompra"]);
                            }
                            $this->obraItem->addHora($pase,$addtipo);
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Recuperar obra(s)
     *
     * @return array
     */
    public function readAction() {
        if(isset($this->params["agenda"])) {
            $options = array(
                "limit" => 0,
                "start" => 0,
                "filter" => "fecha >= NOW()",
                "order" => "fecha, o.nombre",
                );
            $options = array_merge($options,$this->params);
            $data = $this->obraItem->listObras($options);
        } else {
            if(isset($this->params["idobrateatro"]) && !empty($this->params["idobrateatro"])) {
                $data = $this->obraItem->dataObra($this->params["idobrateatro"]);
                $horas = $this->obraItem->listHoras($data[0]);
                $data[0]["horasLocal"] = $horas["horasLocal"];
                $data[0]["horasAuditorio"] = $horas["horasAuditorio"];
                $data[0]["horasCine"] = $horas["horasCine"];
                $data[0]["horasLugar"] = $horas["horasLugar"];
                $data[0]["horasMuseo"] = $horas["horasMuseo"];
            } else {
                $options = array(
                    "limit" => 0,
                    "start" => 0,
                    "filter" => "",
                    "order" => "fecha desc",
                    );
                if(!empty($this->params)) {
                    $options = array_merge($options,$this->params);
                }
                $data = $this->obraItem->listObras($options);
            }
        }

        return $data;
    }

    /**
     * Actualizar obra
     *
     * @return boolean
     */
    public function updateAction() {
        if(isset($this->params["op"])) {
            //Publicar o no publicar
            if(strtolower($this->params["op"]) == "publish") {
                $result = $this->obraItem->publishObra($this->params["ids"]);
            } elseif(strtolower($this->params["op"]) == "unpublish") {
                $result = $this->obraItem->unpublishObra($this->params["ids"]);
            } elseif(strtolower($this->params["op"]) == "field") {
                $fields = array();
                foreach($this->params as $key => $value){
                    $fields[$key] = $value;
                }
                unset($fields["op"]);
                $result = $this->obraItem->updateObra($fields);
            } else {
                $result = false;
            }
        } else {
            $obra = array();
            foreach($this->params as $key => $value){
                $obra[$key] = $value;
            }

            //Horarios
            $bdhorarios = $this->obraItem->listHoras(array("idobrateatro" => $obra["idobrateatro"]));
            $tipohorarios = array("horasLocal", "horasAuditorio", "horasCine", "horasLugar", "horasMuseo");
            foreach($tipohorarios as $th) {
                $bdhoras = $bdhorarios[$th];
                switch($th) {
                    case "horasLocal":
                        $formfield = "locales";
                        $idtipo = "idlocal";
                        $addtipo = "local";
                        break;
                    case "horasAuditorio":
                        $formfield = "auditorios";
                        $idtipo = "idteatro";
                        $addtipo = "teatro";
                        break;
                    case "horasCine":
                        $formfield = "cines";
                        $idtipo = "idcine";
                        $addtipo = "cine";
                        break;
                    case "horasLugar":
                        $formfield = "lugar";
                        $idtipo = "";
                        $addtipo = "lugar";
                        break;
                    case "horasMuseo":
                        $formfield = "museos";
                        $idtipo = "idmuseo";
                        $addtipo = "museo";
                        break;
                }
                if(isset($obra[$formfield])) {
                    $horarios = $obra[$formfield];
                } else {
                    $horarios = array();
                }

                foreach($horarios as $key => $local) {
                    foreach($local as $k => $hora) {
                        if(isset($local[0][$idtipo])) {
                            $id = $local[0][$idtipo];
                        } else {
                            $id = "";
                        }
                        if(isset($hora["fecha"])) {
                            $pase = array(
                                "idlugar" => $id,
                                "idobrateatro" => $obra["idobrateatro"],
                                "fecha" => date("Y-m-d H:i",strtotime($hora["fecha"])),
                                "precio" => $hora["precio"],
                                "agotado" => array_key_exists("agotado",$hora)? 1:0,
                                "cancelado" => array_key_exists("cancelado",$hora)? 1:0,
                                "todoeldia" => array_key_exists("tododia",$hora)? 1:0
                                );
                            if($addtipo == "cine") {
                                $pase["idsala"] = $hora["idsala"];
                                $pase["3d"] = array_key_exists("3d",$hora)? 1:0;
                                $pase["vo"] = array_key_exists("vo",$hora)? 1:0;
                            } else {
                                $pase["precioanticipada"] = $hora["precioanticipada"];
                            }
                            if($addtipo == "lugar") {
                                $pase["coordenadas"] = trim($horarios[0][0]["coordenadas"]);
                                $pase["lugar"] = trim($hora["lugar"]);
                                $pase["localidad"] = trim($hora["localidad"]);
                            } else {
                                $pase["urlcompra"] = trim($hora["urlcompra"]);
                            }
                            $existe = false;
                            //Comprobamos si existe en la BD para modificarlo, en vez de darlo de alta de nuevo
                            foreach($bdhoras as $ke => $val) {
                                if(isset($val[$idtipo])) {
                                    if($val[$idtipo] == $pase["idlugar"] && $val["idobrateatro"] == $pase["idobrateatro"] && date("Y-m-d H:i",strtotime($val["fecha"])) == $pase["fecha"]) {
                                        $existe = true;
                                        unset($bdhoras[$ke]);
                                        break;
                                    }
                                } else {
                                    if($val["idobrateatro"] == $pase["idobrateatro"] && date("Y-m-d H:i",strtotime($val["fecha"])) == date("Y-m-d H:i",strtotime($pase["fecha"]))) {
                                        $existe = true;
                                        unset($bdhoras[$ke]);
                                        break;
                                    }
                                }
                            }
                            if($existe) {
                                $this->obraItem->updateHora($pase,$addtipo);
                            } else {
                                $this->obraItem->addHora($pase,$addtipo);
                            }
                        }
                    }
                }
                foreach($bdhoras as $delHora) {
                    if(isset($delHora[$idtipo])) {
                        $delHora["idlugar"] = $delHora[$idtipo];
                    }
                    $this->obraItem->deleteHora($delHora,$addtipo);
                }
                unset($obra[$formfield]);
            }

            $obra["publicado"] = array_key_exists("publicado",$obra)? 1:0;
            $obra["infantil"] = array_key_exists("infantil",$obra)? 1:0;
            $obra["reserva"] = array_key_exists("reserva",$obra)? 1:0;

            $result = $this->obraItem->updateObra($obra);
        }

        return $result;
    }

    /**
     * Eliminar obra
     *
     * @return boolean
     */
    public function deleteAction() {
        if(isset($this->params["image"])) {
            $result = $this->general->deleteImage($this->params["id"], $this->params["image"]);
        } else {
            $result = $this->obraItem->deleteObra($this->params["id"]);
        }

        return $result;
    }
}