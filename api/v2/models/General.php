<?php
/**
 * Clase con metodos para operaciones comunes a varios modelos
 *
 * @package API
 * @author Trasno
 */
class General {
    protected $db;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct(Db $db) {
        $this->db = $db;
    }

    /**
     * Coge todos los eventos del dia actual
     *
     * @param array $options
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro para nombre de usuario. El nombre de usuario tiene que contener esta cadena.
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listAgendaHoy($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "and ".$options["filter"];
        }
        $between = explode(" ",$options["filter"]);
        preg_match("/= date\('(.*?)'\)/", $options["filter"], $fil);
        if(isset($fil[1])) {
            $f = explode(" ",$fil[1]);
            $now = $f[0]." 23:59:59";
        } else {
            $now = date("Y-m-d", strtotime("now"))." 23:59:59";
        }
        if(date("G", strtotime("now")) > 5) {
            $expoFilter = "((inicio <= ".$between[3]."' and fin >= ".$between[3]."') or permanente = 1) and WEEKDAY(".$between[3]."')+1 not in (select cerrado from horarios where tipolugar='museo' and idlugar=me.idmuseo)";
            if(strpos($options["filter"],"infantil")) {
                $expoFilter .= " and infantil = 1";
            }
        } else {
            $expoFilter = "0";
        }
        $query = sprintf("select id, nombre, genero, poster, fecha, infantil, agotado, cancelado, todoeldia, tipoevento, reserva, localidad from (

        select c.idconcierto as id, nombre, genero, poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos' as tipoevento, reserva, localidad from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia, localidad from %slocales_has_conciertos lc, %slocales l where lc.idlocal = l.idlocal order by fecha asc) as lc where lc.idconcierto = c.idconcierto %s union
        select c.idconcierto, nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos', reserva, localidad from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia, localidad from %steatros_has_conciertos lc, %steatros l where lc.idteatro = l.idteatro order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s union
        select c.idconcierto, nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos', reserva, localidad from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia, localidad from %scines_has_conciertos lc, %scines l where lc.idcine = l.idcine order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s union
        select c.idconcierto, nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos', reserva, localidad from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia, localidad from %slugares_has_conciertos order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s union
        select c.idconcierto, nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos', reserva, localidad from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia, localidad from %smuseos_has_conciertos lc, %smuseos l where lc.idmuseo = l.idmuseo order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s union

        select p.idpelicula, nombre, genero, poster, cp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', reserva, localidad from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia, localidad from %scines_has_peliculas lc, %scines l where lc.idcine = l.idcine order by fecha asc) as cp where cp.idpelicula = p.idpelicula %s union
        select p.idpelicula, nombre, genero, poster, lp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', reserva, localidad from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia, localidad from %slocales_has_peliculas lc, %slocales l where lc.idlocal = l.idlocal order by fecha asc) as lp where lp.idpelicula = p.idpelicula %s union
        select p.idpelicula, nombre, genero, poster, tp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', reserva, localidad from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia, localidad from %steatros_has_peliculas lc, %steatros l where lc.idteatro = l.idteatro order by fecha asc) as tp where tp.idpelicula = p.idpelicula %s union
        select p.idpelicula, nombre, genero, poster, tp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', reserva, localidad from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia, localidad from %slugares_has_peliculas order by fecha asc) as tp where tp.idpelicula = p.idpelicula %s union
        select p.idpelicula, nombre, genero, poster, tp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', reserva, localidad from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia, localidad from %smuseos_has_peliculas lc, %smuseos l where lc.idmuseo = l.idmuseo order by fecha asc) as tp where tp.idpelicula = p.idpelicula %s union

        select t.idobrateatro, nombre, genero, poster, lt.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', reserva, localidad from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia, localidad from %slocales_has_obrasteatro lc, %slocales l where lc.idlocal = l.idlocal order by fecha asc) as lt where lt.idobrateatro = t.idobrateatro %s union
        select t.idobrateatro, nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', reserva, localidad from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia, localidad from %steatros_has_obrasteatro lc, %steatros l where lc.idteatro = l.idteatro order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s union
        select t.idobrateatro, nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', reserva, localidad from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia, localidad from %scines_has_obrasteatro lc, %scines l where lc.idcine = l.idcine order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s union
        select t.idobrateatro, nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', reserva, localidad from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia, localidad from %slugares_has_obrasteatro order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s union
        select t.idobrateatro, nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', reserva, localidad from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia, localidad from %smuseos_has_obrasteatro lc, %smuseos l where lc.idmuseo = l.idmuseo order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s union

        select e.ideventos, nombre, tipo, poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', reserva, localidad from %seventos e, (select le.ideventos, fecha, agotado, cancelado, todoeldia, localidad from %slocales_has_eventos le, %slocales l where le.idlocal = l.idlocal order by fecha asc) as le where le.ideventos = e.ideventos %s union
        select e.ideventos, nombre, tipo, poster, te.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', reserva, localidad from %seventos e, (select te.ideventos, fecha, agotado, cancelado, todoeldia, localidad from %steatros_has_eventos te, %steatros l where te.idteatro = l.idteatro order by fecha asc) as te where te.ideventos = e.ideventos %s union
        select e.ideventos, nombre, tipo, poster, te.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', reserva, localidad from %seventos e, (select te.ideventos, fecha, agotado, cancelado, todoeldia, localidad from %scines_has_eventos te, %scines l where te.idcine = l.idcine order by fecha asc) as te where te.ideventos = e.ideventos %s union
        select e.ideventos, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', reserva, localidad from %seventos e, (select lue.ideventos, fecha, agotado, cancelado, todoeldia, localidad from %slugares_has_eventos lue order by fecha asc) as lue where lue.ideventos = e.ideventos %s union
        select e.ideventos, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', reserva, localidad from %seventos e, (select lue.ideventos, fecha, agotado, cancelado, todoeldia, localidad from %smuseos_has_eventos lue, %smuseos l where lue.idmuseo = l.idmuseo order by fecha asc) as lue where lue.ideventos = e.ideventos %s union

        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', reserva, localidad from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia, localidad from %slocales_has_deportes le, %slocales l where le.idlocal = l.idlocal order by fecha asc) as le where le.iddeporte = e.iddeporte %s union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', reserva, localidad from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia, localidad from %steatros_has_deportes le, %steatros l where le.idteatro = l.idteatro order by fecha asc) as le where le.iddeporte = e.iddeporte %s union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', reserva, localidad from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia, localidad from %slugares_has_deportes le order by fecha asc) as le where le.iddeporte = e.iddeporte %s union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', reserva, localidad from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia, localidad from %scines_has_deportes le, %scines l where le.idcine = l.idcine order by fecha asc) as le where le.iddeporte = e.iddeporte %s union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', reserva, localidad from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia, localidad from %smuseos_has_deportes le, %smuseos l where le.idmuseo = l.idmuseo order by fecha asc) as le where le.iddeporte = e.iddeporte %s union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', reserva, localidad from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia, localidad from %spabellones_has_deportes le, %spabellones l where le.idpabellon = l.idpabellon order by fecha asc) as le where le.iddeporte = e.iddeporte %s union

        select e.idcurso, nombre, tipo, poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', reserva, localidad from %scursos e, (select le.idcurso, fecha, agotado, cancelado, todoeldia, localidad from %slocales_has_cursos le, %slocales l where le.idlocal = l.idlocal order by fecha asc) as le where le.idcurso = e.idcurso %s union
        select e.idcurso, nombre, tipo, poster, te.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', reserva, localidad from %scursos e, (select te.idcurso, fecha, agotado, cancelado, todoeldia, localidad from %steatros_has_cursos te, %steatros l where te.idteatro = l.idteatro order by fecha asc) as te where te.idcurso = e.idcurso %s union
        select e.idcurso, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', reserva, localidad from %scursos e, (select lue.idcurso, fecha, agotado, cancelado, todoeldia, localidad from %slugares_has_cursos lue order by fecha asc) as lue where lue.idcurso = e.idcurso %s union
        select e.idcurso, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', reserva, localidad from %scursos e, (select lue.idcurso, fecha, agotado, cancelado, todoeldia, localidad from %scines_has_cursos lue, %scines l where lue.idcine = l.idcine order by fecha asc) as lue where lue.idcurso = e.idcurso %s union
        select e.idcurso, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', reserva, localidad from %scursos e, (select lue.idcurso, fecha, agotado, cancelado, todoeldia, localidad from %smuseos_has_cursos lue, %smuseos l where lue.idmuseo = l.idmuseo order by fecha asc) as lue where lue.idcurso = e.idcurso %s union

        select e.idexposicion, e.nombre, genero, poster, '".$now."', infantil, agotado, cancelado, '1', 'exposiciones', 0, localidad from %sexposiciones e, %smuseos_has_exposiciones me, %smuseos l where me.idmuseo = l.idmuseo and me.idexposicion = e.idexposicion and %s and e.publicado = 1 union
        select e.idexposicion, e.nombre, genero, poster, '".$now."', infantil, agotado, cancelado, '1', 'exposiciones', 0, localidad from %sexposiciones e, %slocales_has_exposiciones me, %slocales l where me.idlocal = l.idlocal and me.idexposicion = e.idexposicion and %s and e.publicado = 1 union
        select e.idexposicion, e.nombre, genero, poster, '".$now."', infantil, agotado, cancelado, '1', 'exposiciones', 0, localidad from %sexposiciones e, %steatros_has_exposiciones me, %steatros l where me.idteatro = l.idteatro and me.idexposicion = e.idexposicion and %s and e.publicado = 1 union
        select e.idexposicion, e.nombre, genero, poster, '".$now."', infantil, agotado, cancelado, '1', 'exposiciones', 0, localidad from %sexposiciones e, %slugares_has_exposiciones me where me.idexposicion = e.idexposicion and %s and e.publicado = 1

        ) as consulta
        order by %s %s",
        BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX,$options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX,$expoFilter,
        BDPREFIX, BDPREFIX, BDPREFIX,str_replace("idmuseo", "idlocal", $expoFilter),
        BDPREFIX, BDPREFIX, BDPREFIX,str_replace("idmuseo", "idteatro", $expoFilter),
        BDPREFIX, BDPREFIX ,str_replace(" and idlugar=me.idmuseo", "", $expoFilter),
        $this->db->secure_field($options["order"]),$this->db->secure_field($limit));

        //file_put_contents(BASE_URI."temp/logfile.txt", $query);
        $r = $this->db->query($query);

        $result = array();
        while($pelicula = $this->db->fetch($r)) {
            $result[] = $pelicula;
        }

        return $result;
    }

    /**
     * Coge todos los eventos de un lugar
     *
     * @param array $options
     * @param int $options["idlugar"] id de lugar.
     * @param int $options["tipolugar"] id de lugar.
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro.
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listNextEvents($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "and ".$options["filter"];
        }
        $expoFilter = "";
        if(strpos($options["filter"],"fecha >= ") === false) {
            $expoFilter = " and fin < CURDATE() and permanente <> 1";
        } else {
            $expoFilter = " and (fin >= CURDATE() or permanente = 1)";
        }
        switch($options["tipolugar"]) {
            case "local":
                $subquery = sprintf("
                select c.idconcierto as id, nombre, genero, poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos' as tipoevento, creado, actualizado from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %slocales_has_conciertos where idlocal = %d order by fecha asc) as lc where lc.idconcierto = c.idconcierto %s group by lc.fecha, c.idconcierto union
                select p.idpelicula, nombre, genero, poster, lp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', creado, actualizado from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %slocales_has_peliculas where idlocal = %d order by fecha asc) as lp where lp.idpelicula = p.idpelicula %s group by date(lp.fecha), p.idpelicula union
                select t.idobrateatro, nombre, genero, poster, lt.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', creado, actualizado from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %slocales_has_obrasteatro where idlocal = %d order by fecha asc) as lt where lt.idobrateatro = t.idobrateatro %s group by lt.fecha, t.idobrateatro union
                select e.ideventos, nombre, tipo, poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', creado, actualizado from %seventos e, (select le.ideventos, fecha, agotado, cancelado, todoeldia from %slocales_has_eventos le where idlocal = %d order by fecha asc) as le where le.ideventos = e.ideventos %s group by le.fecha, e.ideventos union
                select c.iddeporte as id, c.nombre, td.idtipodeporte as genero, c.poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'deportes' as tipoevento, creado, actualizado from %sdeportes c LEFT JOIN %stipos_deporte td on td.idtipodeporte = c.tipo, (select lc.iddeporte, fecha, agotado, cancelado, todoeldia from %slocales_has_deportes lc where idlocal = %d order by fecha asc) as lc where lc.iddeporte = c.iddeporte %s group by lc.fecha, c.iddeporte union
                select c.idcurso, nombre, tipo, poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', creado, actualizado from %scursos c, (select lc.idcurso, fecha, agotado, cancelado, todoeldia from %slocales_has_cursos lc where idlocal = %d order by fecha asc) as lc where lc.idcurso = c.idcurso %s group by lc.fecha, c.idcurso union
                select e.idexposicion, nombre, genero, poster, DATE_FORMAT(fin,'%%d %%b %%Y 00:00'), infantil, agotado, cancelado, '1', 'exposiciones', creado, actualizado from %sexposiciones e, %slocales_has_exposiciones me where me.idlocal = %d and me.idexposicion = e.idexposicion %s and publicado = 1
                ",
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $expoFilter);
                break;
            case "cine":
                $subquery = sprintf("
                select c.idconcierto as id, nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos' as tipoevento, creado, actualizado from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %scines_has_conciertos where idcine = %d order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s group by tc.fecha, c.idconcierto union
                select p.idpelicula, nombre, genero, poster, cp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', creado, actualizado from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %scines_has_peliculas where idcine = %d order by fecha asc) as cp where cp.idpelicula = p.idpelicula %s group by date(cp.fecha), p.idpelicula union
                select t.idobrateatro, nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', creado, actualizado from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %scines_has_obrasteatro where idcine = %d order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s group by at.fecha, t.idobrateatro union
                select e.ideventos, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', creado, actualizado from %seventos e, (select lue.ideventos, fecha, agotado, cancelado, todoeldia from %scines_has_eventos lue where idcine = %d order by fecha asc) as lue where lue.ideventos = e.ideventos %s group by lue.fecha, e.ideventos union
                select c.iddeporte as id, c.nombre, td.idtipodeporte as genero, c.poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'deportes' as tipoevento, creado, actualizado from %sdeportes c LEFT JOIN %stipos_deporte td on td.idtipodeporte = c.tipo, (select lc.iddeporte, fecha, agotado, cancelado, todoeldia from %scines_has_deportes lc where idcine = %d order by fecha asc) as lc where lc.iddeporte = c.iddeporte %s group by lc.fecha, c.iddeporte union
                select c.idcurso, nombre, tipo, poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', creado, actualizado from %scursos c, (select lc.idcurso, fecha, agotado, cancelado, todoeldia from %scines_has_cursos lc where idcine = %d order by fecha asc) as lc where lc.idcurso = c.idcurso %s group by lc.fecha, c.idcurso
                ",
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"]);
                break;
            case "teatro":
                $subquery = sprintf("
                select c.idconcierto as id, nombre, genero, poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos' as tipoevento, creado, actualizado from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %steatros_has_conciertos where idteatro = %d order by fecha asc) as lc where lc.idconcierto = c.idconcierto %s group by lc.fecha, c.idconcierto union
                select p.idpelicula, nombre, genero, poster, lp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', creado, actualizado from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %steatros_has_peliculas where idteatro = %d order by fecha asc) as lp where lp.idpelicula = p.idpelicula %s group by date(lp.fecha), p.idpelicula union
                select t.idobrateatro, nombre, genero, poster, lt.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', creado, actualizado from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %steatros_has_obrasteatro where idteatro = %d order by fecha asc) as lt where lt.idobrateatro = t.idobrateatro %s group by lt.fecha, t.idobrateatro union
                select e.ideventos, nombre, tipo, poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', creado, actualizado from %seventos e, (select le.ideventos, fecha, agotado, cancelado, todoeldia from %steatros_has_eventos le where idteatro = %d order by fecha asc) as le where le.ideventos = e.ideventos %s group by le.fecha, e.ideventos union
                select c.iddeporte as id, c.nombre, td.idtipodeporte as genero, c.poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'deportes' as tipoevento, creado, actualizado from %sdeportes c LEFT JOIN %stipos_deporte td on td.idtipodeporte = c.tipo, (select lc.iddeporte, fecha, agotado, cancelado, todoeldia from %steatros_has_deportes lc where idteatro = %d order by fecha asc) as lc where lc.iddeporte = c.iddeporte %s group by lc.fecha, c.iddeporte union
                select c.idcurso, nombre, tipo, poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', creado, actualizado from %scursos c, (select lc.idcurso, fecha, agotado, cancelado, todoeldia from %steatros_has_cursos lc where idteatro = %d order by fecha asc) as lc where lc.idcurso = c.idcurso %s group by lc.fecha, c.idcurso union
                select e.idexposicion, nombre, genero, poster, DATE_FORMAT(fin,'%%d %%b %%Y 00:00'), infantil, agotado, cancelado, '1', 'exposiciones', creado, actualizado from %sexposiciones e, %steatros_has_exposiciones me where me.idteatro = %d and me.idexposicion = e.idexposicion %s and publicado = 1
                ",
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $expoFilter);
                break;
            case "museo":
                $subquery = sprintf("
                select c.idconcierto as id, nombre, genero, poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos' as tipoevento, creado, actualizado from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %smuseos_has_conciertos where idmuseo = %d order by fecha asc) as lc where lc.idconcierto = c.idconcierto %s group by lc.fecha, c.idconcierto union
                select p.idpelicula, nombre, genero, poster, lp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', creado, actualizado from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %smuseos_has_peliculas where idmuseo = %d order by fecha asc) as lp where lp.idpelicula = p.idpelicula %s group by date(lp.fecha), p.idpelicula union
                select t.idobrateatro, nombre, genero, poster, lt.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', creado, actualizado from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %smuseos_has_obrasteatro where idmuseo = %d order by fecha asc) as lt where lt.idobrateatro = t.idobrateatro %s group by lt.fecha, t.idobrateatro union
                select e.ideventos, nombre, tipo, poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', creado, actualizado from %seventos e, (select le.ideventos, fecha, agotado, cancelado, todoeldia from %smuseos_has_eventos le where idmuseo = %d order by fecha asc) as le where le.ideventos = e.ideventos %s group by le.fecha, e.ideventos union
                select c.iddeporte as id, c.nombre, td.idtipodeporte as genero, c.poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'deportes' as tipoevento, creado, actualizado from %sdeportes c LEFT JOIN %stipos_deporte td on td.idtipodeporte = c.tipo, (select lc.iddeporte, fecha, agotado, cancelado, todoeldia from %smuseos_has_deportes lc where idmuseo = %d order by fecha asc) as lc where lc.iddeporte = c.iddeporte %s group by lc.fecha, c.iddeporte union
                select c.idcurso, nombre, tipo, poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', creado, actualizado from %scursos c, (select lc.idcurso, fecha, agotado, cancelado, todoeldia from %smuseos_has_cursos lc where idmuseo = %d order by fecha asc) as lc where lc.idcurso = c.idcurso %s group by lc.fecha, c.idcurso union
                select e.idexposicion, nombre, genero, poster, DATE_FORMAT(fin,'%%d %%b %%Y 00:00'), infantil, agotado, cancelado, '1', 'exposiciones', creado, actualizado from %sexposiciones e, %smuseos_has_exposiciones me where me.idmuseo = %d and me.idexposicion = e.idexposicion %s and publicado = 1
                ",
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $expoFilter);
                break;
            case "pabellon":
                $subquery = sprintf("
                select c.iddeporte as id, c.nombre, td.idtipodeporte as genero, c.poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'deportes' as tipoevento, creado, actualizado from %sdeportes c LEFT JOIN %stipos_deporte td on td.idtipodeporte = c.tipo, (select lc.iddeporte, fecha, agotado, cancelado, todoeldia from %spabellones_has_deportes lc where idpabellon = %d order by fecha asc) as lc where lc.iddeporte = c.iddeporte %s group by lc.fecha, c.iddeporte
                ",
                BDPREFIX, BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"]);
                break;
        }

        $query = sprintf("select id, nombre, genero, poster, fecha, infantil, agotado, cancelado, tipoevento, creado, actualizado from (%s) as consulta order by %s %s",

        $subquery,
        $this->db->secure_field($options["order"]),$this->db->secure_field($limit));

        //echo $query."</br>";
        $r = $this->db->query($query);

        $result = array();
        while($pelicula = $this->db->fetch($r)) {
            $result[] = $pelicula;
        }

        return $result;
    }

    /**
     * Coge todas las novedades o actualizaciones de eventos
     *
     * @param array $options
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro.
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listNovedades($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "and ".$options["filter"];
        }
        $query = sprintf("select id, nombre, genero, poster, fecha, infantil, agotado, cancelado, todoeldia, tipoevento, creado, actualizado, reserva, limitereserva from (

        select c.idconcierto as id, nombre, genero, poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos' as tipoevento, creado, actualizado, reserva, limitereserva from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %slocales_has_conciertos order by fecha asc) as lc where lc.idconcierto = c.idconcierto %s group by lc.fecha, c.idconcierto union
        select c.idconcierto, nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos', creado, actualizado, reserva, limitereserva from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %steatros_has_conciertos order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s group by tc.fecha, c.idconcierto union
        select c.idconcierto, nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos', creado, actualizado, reserva, limitereserva from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %scines_has_conciertos order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s group by tc.fecha, c.idconcierto union
        select c.idconcierto, nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos', creado, actualizado, reserva, limitereserva from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %slugares_has_conciertos order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s group by tc.fecha, c.idconcierto union
        select c.idconcierto, nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos', creado, actualizado, reserva, limitereserva from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %smuseos_has_conciertos order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s group by tc.fecha, c.idconcierto union

        select p.idpelicula, nombre, genero, poster, cp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', creado, actualizado, reserva, limitereserva from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %scines_has_peliculas order by fecha asc) as cp where cp.idpelicula = p.idpelicula %s group by date(cp.fecha), p.idpelicula union
        select p.idpelicula, nombre, genero, poster, lp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', creado, actualizado, reserva, limitereserva from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %slocales_has_peliculas order by fecha asc) as lp where lp.idpelicula = p.idpelicula %s group by date(lp.fecha), p.idpelicula union
        select p.idpelicula, nombre, genero, poster, tp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', creado, actualizado, reserva, limitereserva from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %steatros_has_peliculas order by fecha asc) as tp where tp.idpelicula = p.idpelicula %s group by date(tp.fecha), p.idpelicula union
        select p.idpelicula, nombre, genero, poster, tp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', creado, actualizado, reserva, limitereserva from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %slugares_has_peliculas order by fecha asc) as tp where tp.idpelicula = p.idpelicula %s group by date(tp.fecha), p.idpelicula union
        select p.idpelicula, nombre, genero, poster, tp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', creado, actualizado, reserva, limitereserva from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %smuseos_has_peliculas order by fecha asc) as tp where tp.idpelicula = p.idpelicula %s group by date(tp.fecha), p.idpelicula union

        select t.idobrateatro, nombre, genero, poster, lt.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', creado, actualizado, reserva, limitereserva from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %slocales_has_obrasteatro order by fecha asc) as lt where lt.idobrateatro = t.idobrateatro %s group by lt.fecha, t.idobrateatro union
        select t.idobrateatro, nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', creado, actualizado, reserva, limitereserva from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %steatros_has_obrasteatro order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s group by at.fecha, t.idobrateatro union
        select t.idobrateatro, nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', creado, actualizado, reserva, limitereserva from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %scines_has_obrasteatro order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s group by at.fecha, t.idobrateatro union
        select t.idobrateatro, nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', creado, actualizado, reserva, limitereserva from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %slugares_has_obrasteatro order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s group by at.fecha, t.idobrateatro union
        select t.idobrateatro, nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', creado, actualizado, reserva, limitereserva from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %smuseos_has_obrasteatro order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s group by at.fecha, t.idobrateatro union

        select e.ideventos, nombre, tipo, poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', creado, actualizado, reserva, limitereserva from %seventos e, (select le.ideventos, fecha, agotado, cancelado, todoeldia from %slocales_has_eventos le order by fecha asc) as le where le.ideventos = e.ideventos %s group by le.fecha, e.ideventos union
        select e.ideventos, nombre, tipo, poster, te.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', creado, actualizado, reserva, limitereserva from %seventos e, (select te.ideventos, fecha, agotado, cancelado, todoeldia from %steatros_has_eventos te order by fecha asc) as te where te.ideventos = e.ideventos %s group by te.fecha, e.ideventos union
        select e.ideventos, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', creado, actualizado, reserva, limitereserva from %seventos e, (select lue.ideventos, fecha, agotado, cancelado, todoeldia from %slugares_has_eventos lue order by fecha asc) as lue where lue.ideventos = e.ideventos %s group by lue.fecha, e.ideventos union
        select e.ideventos, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', creado, actualizado, reserva, limitereserva from %seventos e, (select lue.ideventos, fecha, agotado, cancelado, todoeldia from %scines_has_eventos lue order by fecha asc) as lue where lue.ideventos = e.ideventos %s group by lue.fecha, e.ideventos union
        select e.ideventos, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', creado, actualizado, reserva, limitereserva from %seventos e, (select lue.ideventos, fecha, agotado, cancelado, todoeldia from %smuseos_has_eventos lue order by fecha asc) as lue where lue.ideventos = e.ideventos %s group by lue.fecha, e.ideventos union

        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', creado, actualizado, reserva, limitereserva from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia from %slocales_has_deportes le order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', creado, actualizado, reserva, limitereserva from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia from %steatros_has_deportes le order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', creado, actualizado, reserva, limitereserva from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia from %slugares_has_deportes le order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', creado, actualizado, reserva, limitereserva from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia from %scines_has_deportes le order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', creado, actualizado, reserva, limitereserva from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia from %smuseos_has_deportes le order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', creado, actualizado, reserva, limitereserva from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia from %spabellones_has_deportes le order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union

        select e.idcurso, nombre, tipo, poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', creado, actualizado, reserva, limitereserva from %scursos e, (select le.idcurso, fecha, agotado, cancelado, todoeldia from %slocales_has_cursos le order by fecha asc) as le where le.idcurso = e.idcurso %s group by le.fecha, e.idcurso union
        select e.idcurso, nombre, tipo, poster, te.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', creado, actualizado, reserva, limitereserva from %scursos e, (select te.idcurso, fecha, agotado, cancelado, todoeldia from %steatros_has_cursos te order by fecha asc) as te where te.idcurso = e.idcurso %s group by te.fecha, e.idcurso union
        select e.idcurso, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', creado, actualizado, reserva, limitereserva from %scursos e, (select lue.idcurso, fecha, agotado, cancelado, todoeldia from %slugares_has_cursos lue order by fecha asc) as lue where lue.idcurso = e.idcurso %s group by lue.fecha, e.idcurso union
        select e.idcurso, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', creado, actualizado, reserva, limitereserva from %scursos e, (select lue.idcurso, fecha, agotado, cancelado, todoeldia from %scines_has_cursos lue order by fecha asc) as lue where lue.idcurso = e.idcurso %s group by lue.fecha, e.idcurso union
        select e.idcurso, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', creado, actualizado, reserva, limitereserva from %scursos e, (select lue.idcurso, fecha, agotado, cancelado, todoeldia from %smuseos_has_cursos lue order by fecha asc) as lue where lue.idcurso = e.idcurso %s group by lue.fecha, e.idcurso union

        select e.idexposicion, nombre, genero, poster, concat(CURDATE() + INTERVAL 1 DAY,' 01:59'), infantil, agotado, cancelado, '1', 'exposiciones', creado, actualizado, reserva, limitereserva from %sexposiciones e, %smuseos_has_exposiciones me where me.idexposicion = e.idexposicion %s and publicado = 1 union
        select e.idexposicion, nombre, genero, poster, concat(CURDATE() + INTERVAL 1 DAY,' 01:59'), infantil, agotado, cancelado, '1', 'exposiciones', creado, actualizado, reserva, limitereserva from %sexposiciones e, %slocales_has_exposiciones me where me.idexposicion = e.idexposicion %s and publicado = 1 union
        select e.idexposicion, nombre, genero, poster, concat(CURDATE() + INTERVAL 1 DAY,' 01:59'), infantil, agotado, cancelado, '1', 'exposiciones', creado, actualizado, reserva, limitereserva from %sexposiciones e, %steatros_has_exposiciones me where me.idexposicion = e.idexposicion %s and publicado = 1 union
        select e.idexposicion, nombre, genero, poster, concat(CURDATE() + INTERVAL 1 DAY,' 01:59'), infantil, agotado, cancelado, '1', 'exposiciones', creado, actualizado, reserva, limitereserva from %sexposiciones e, %slugares_has_exposiciones me where me.idexposicion = e.idexposicion %s and publicado = 1

        ) as consulta group by date(actualizado), nombre
        order by %s %s",
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, str_replace("fecha >= now() and ","",$options["filter"]),
        BDPREFIX, BDPREFIX, str_replace("fecha >= now() and ","",$options["filter"]),
        BDPREFIX, BDPREFIX, str_replace("fecha >= now() and ","",$options["filter"]),
        BDPREFIX, BDPREFIX, str_replace("fecha >= now() and ","",$options["filter"]),
        $this->db->secure_field($options["order"]),$this->db->secure_field($limit));
        //echo $query."</br>";
        //file_put_contents(BASE_URI."temp/pp.txt", $query);
        $r = $this->db->query($query);

        $result = array();
        while($pelicula = $this->db->fetch($r)) {
            $result[] = $pelicula;
        }

        return $result;
    }

    /**
     * Coge todos los eventos de hoy y sus coordenadas
     *
     * @param array $options
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro para nombre de usuario. El nombre de usuario tiene que contener esta cadena.
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listAgendaHoyCoord($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "and ".$options["filter"];
        }
        $between = explode(" ",$options["filter"]);
        preg_match("/= date\('(.*?)'\)/", $options["filter"], $fil);
        if(isset($fil[1])) {
            $f = explode(" ",$fil[1]);
            $now = $f[0]." 23:59:59";
        } else {
            $now = date("Y-m-d", strtotime("now"))." 23:59:59";
        }
        if(date("G", strtotime("now")) > 5) {
            $expoFilter = "((inicio <= ".$between[3]."' and fin >= ".$between[3]."') or permanente = 1) and WEEKDAY(".$between[3]."')+1 not in (select cerrado from horarios where tipolugar='museo' and idlugar=me.idmuseo)";
            if(strpos($options["filter"],"infantil")) {
                $expoFilter .= " and infantil = 1";
            }
        } else {
            $expoFilter = "0";
        }
        $query = sprintf("select id, nombre, genero, poster, fecha, infantil, agotado, cancelado, todoeldia, tipoevento, reserva, coordenadas from (

        select c.idconcierto as id, c.nombre, genero, poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos' as tipoevento, reserva, coordenadas from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia, coordenadas from %slocales_has_conciertos lc, %slocales l where lc.idlocal = l.idlocal order by fecha asc) as lc where lc.idconcierto = c.idconcierto %s union
        select c.idconcierto, c.nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos', reserva, coordenadas from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia, coordenadas from %steatros_has_conciertos lo, %steatros l where lo.idteatro = l.idteatro order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s union
        select c.idconcierto, c.nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos', reserva, coordenadas from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia, coordenadas from %scines_has_conciertos lo, %scines l where lo.idcine = l.idcine order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s union
        select c.idconcierto, c.nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos', reserva, coordenadas from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia, coordenadas from %slugares_has_conciertos order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s union
        select c.idconcierto, c.nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos', reserva, coordenadas from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia, coordenadas from %smuseos_has_conciertos lo, %smuseos l where lo.idmuseo = l.idmuseo order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s union

        select p.idpelicula, p.nombre, genero, poster, cp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', reserva, coordenadas from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia, coordenadas from %scines_has_peliculas lo, %scines l where lo.idcine = l.idcine order by fecha asc) as cp where cp.idpelicula = p.idpelicula %s union
        select p.idpelicula, p.nombre, genero, poster, lp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', reserva, coordenadas from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia, coordenadas from %slocales_has_peliculas lc, %slocales l where lc.idlocal = l.idlocal order by fecha asc) as lp where lp.idpelicula = p.idpelicula %s union
        select p.idpelicula, p.nombre, genero, poster, tp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', reserva, coordenadas from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia, coordenadas from %steatros_has_peliculas lo, %steatros l where lo.idteatro = l.idteatro order by fecha asc) as tp where tp.idpelicula = p.idpelicula %s union
        select p.idpelicula, p.nombre, genero, poster, tp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', reserva, coordenadas from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia, coordenadas from %slugares_has_peliculas order by fecha asc) as tp where tp.idpelicula = p.idpelicula %s union
        select p.idpelicula, p.nombre, genero, poster, tp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', reserva, coordenadas from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia, coordenadas from %smuseos_has_peliculas lo, %smuseos l where lo.idmuseo = l.idmuseo order by fecha asc) as tp where tp.idpelicula = p.idpelicula %s union

        select t.idobrateatro, t.nombre, genero, poster, lt.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', reserva, coordenadas from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia, coordenadas from %slocales_has_obrasteatro lc, %slocales l where lc.idlocal = l.idlocal order by fecha asc) as lt where lt.idobrateatro = t.idobrateatro %s union
        select t.idobrateatro, t.nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', reserva, coordenadas from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia, coordenadas from %steatros_has_obrasteatro lo, %steatros l where lo.idteatro = l.idteatro order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s union
        select t.idobrateatro, t.nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', reserva, coordenadas from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia, coordenadas from %scines_has_obrasteatro lo, %scines l where lo.idcine = l.idcine order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s union
        select t.idobrateatro, t.nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', reserva, coordenadas from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia, coordenadas from %slugares_has_obrasteatro order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s union
        select t.idobrateatro, t.nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', reserva, coordenadas from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia, coordenadas from %smuseos_has_obrasteatro lo, %smuseos l where lo.idmuseo = l.idmuseo order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s union

        select e.ideventos, e.nombre, tipo, poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', reserva, coordenadas from %seventos e, (select le.ideventos, fecha, agotado, cancelado, todoeldia, coordenadas from %slocales_has_eventos le, %slocales l where le.idlocal = l.idlocal order by fecha asc) as le where le.ideventos = e.ideventos %s union
        select e.ideventos, e.nombre, tipo, poster, te.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', reserva, coordenadas from %seventos e, (select te.ideventos, fecha, agotado, cancelado, todoeldia, coordenadas from %steatros_has_eventos te, %steatros l where te.idteatro = l.idteatro order by fecha asc) as te where te.ideventos = e.ideventos %s union
        select e.ideventos, e.nombre, tipo, poster, te.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', reserva, coordenadas from %seventos e, (select te.ideventos, fecha, agotado, cancelado, todoeldia, coordenadas from %scines_has_eventos te, %scines l where te.idcine = l.idcine order by fecha asc) as te where te.ideventos = e.ideventos %s union
        select e.ideventos, e.nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', reserva, coordenadas from %seventos e, (select lue.ideventos, fecha, agotado, cancelado, todoeldia, coordenadas from %slugares_has_eventos lue order by fecha asc) as lue where lue.ideventos = e.ideventos %s union
        select e.ideventos, e.nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', reserva, coordenadas from %seventos e, (select lue.ideventos, fecha, agotado, cancelado, todoeldia, coordenadas from %smuseos_has_eventos lue, %smuseos l where lue.idmuseo = l.idmuseo order by fecha asc) as lue where lue.ideventos = e.ideventos %s union

        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', reserva, coordenadas from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia, coordenadas from %slocales_has_deportes le, %slocales l where le.idlocal = l.idlocal order by fecha asc) as le where le.iddeporte = e.iddeporte %s union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', reserva, coordenadas from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia, coordenadas from %steatros_has_deportes le, %steatros l where le.idteatro = l.idteatro order by fecha asc) as le where le.iddeporte = e.iddeporte %s union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', reserva, coordenadas from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia, coordenadas from %slugares_has_deportes le order by fecha asc) as le where le.iddeporte = e.iddeporte %s union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', reserva, coordenadas from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia, coordenadas from %scines_has_deportes le, %scines l where le.idcine = l.idcine order by fecha asc) as le where le.iddeporte = e.iddeporte %s union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', reserva, coordenadas from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia, coordenadas from %smuseos_has_deportes le, %smuseos l where le.idmuseo = l.idmuseo order by fecha asc) as le where le.iddeporte = e.iddeporte %s union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', reserva, coordenadas from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia, coordenadas from %spabellones_has_deportes le, %spabellones l where le.idpabellon = l.idpabellon order by fecha asc) as le where le.iddeporte = e.iddeporte %s union

        select e.idcurso, e.nombre, tipo, poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', reserva, coordenadas from %scursos e, (select le.idcurso, fecha, agotado, cancelado, todoeldia, coordenadas from %slocales_has_cursos le, %slocales l where le.idlocal = l.idlocal order by fecha asc) as le where le.idcurso = e.idcurso %s union
        select e.idcurso, e.nombre, tipo, poster, te.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', reserva, coordenadas from %scursos e, (select te.idcurso, fecha, agotado, cancelado, todoeldia, coordenadas from %steatros_has_cursos te, %steatros l where te.idteatro = l.idteatro order by fecha asc) as te where te.idcurso = e.idcurso %s union
        select e.idcurso, e.nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', reserva, coordenadas from %scursos e, (select lue.idcurso, fecha, agotado, cancelado, todoeldia, coordenadas from %slugares_has_cursos lue order by fecha asc) as lue where lue.idcurso = e.idcurso %s union
        select e.idcurso, e.nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', reserva, coordenadas from %scursos e, (select lue.idcurso, fecha, agotado, cancelado, todoeldia, coordenadas from %scines_has_cursos lue, %scines l where lue.idcine = l.idcine order by fecha asc) as lue where lue.idcurso = e.idcurso %s union
        select e.idcurso, e.nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', reserva, coordenadas from %scursos e, (select lue.idcurso, fecha, agotado, cancelado, todoeldia, coordenadas from %smuseos_has_cursos lue, %smuseos l where lue.idmuseo = l.idmuseo order by fecha asc) as lue where lue.idcurso = e.idcurso %s union

        select e.idexposicion, e.nombre, genero, poster, '".$now."', infantil, agotado, cancelado, '1', 'exposiciones', 0, coordenadas from %sexposiciones e, %smuseos_has_exposiciones me, %smuseos l where me.idmuseo = l.idmuseo and me.idexposicion = e.idexposicion and %s and e.publicado = 1 union
        select e.idexposicion, e.nombre, genero, poster, '".$now."', infantil, agotado, cancelado, '1', 'exposiciones', 0, coordenadas from %sexposiciones e, %slocales_has_exposiciones me, %slocales l where me.idlocal = l.idlocal and me.idexposicion = e.idexposicion and %s and e.publicado = 1 union
        select e.idexposicion, e.nombre, genero, poster, '".$now."', infantil, agotado, cancelado, '1', 'exposiciones', 0, coordenadas from %sexposiciones e, %steatros_has_exposiciones me, %steatros l where me.idteatro = l.idteatro and me.idexposicion = e.idexposicion and %s and e.publicado = 1 union
        select e.idexposicion, e.nombre, genero, poster, '".$now."', infantil, agotado, cancelado, '1', 'exposiciones', 0, coordenadas from %sexposiciones e, %slugares_has_exposiciones me where me.idexposicion = e.idexposicion and %s and e.publicado = 1

        ) as consulta group by nombre, date(fecha)
        order by %s %s",
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $expoFilter,
        BDPREFIX, BDPREFIX, BDPREFIX, str_replace("idmuseo", "idlocal", $expoFilter),
        BDPREFIX, BDPREFIX, BDPREFIX, str_replace("idmuseo", "idteatro", $expoFilter),
        BDPREFIX, BDPREFIX, str_replace(" and idlugar=me.idmuseo", "", $expoFilter),
        $this->db->secure_field($options["order"]),$this->db->secure_field($limit));
        //logFile($query);
        $r = $this->db->query($query);

        $result = array();
        while($pelicula = $this->db->fetch($r)) {
            $result[] = $pelicula;
        }

        return $result;
    }

    /**
     * Coge las localidades que tienen eventos y cuantos tienen
     *
     * @param array $options
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro para nombre de usuario. El nombre de usuario tiene que contener esta cadena.
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listAyuntamientos($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "and ".$options["filter"];
        }
        $now = date("Y-m-d H:i", strtotime("now"));
        $options["filter"] = $now;
        $query = sprintf("select sum(cont) as total, localidad from (

        select count(*) as cont, localidad from %slocales_has_conciertos lc, %slocales l where lc.idlocal=l.idlocal and lc.fecha > '%s' group by l.localidad union all
        select count(*) as cont, localidad from %steatros_has_conciertos lc, %steatros l where lc.idteatro=l.idteatro and lc.fecha > '%s' group by l.localidad union all
        select count(*) as cont, localidad from %scines_has_conciertos lc, %scines l where lc.idcine=l.idcine and lc.fecha > '%s' group by l.localidad union all
        select count(*) as cont, localidad from %smuseos_has_conciertos lc, %smuseos l where lc.idmuseo=l.idmuseo and lc.fecha > '%s' group by l.localidad union all

        select count(*) as cont, localidad from %slocales_has_peliculas lc, %slocales l where lc.idlocal=l.idlocal and lc.fecha > '%s' group by l.localidad union all
        select count(*) as cont, localidad from %steatros_has_peliculas lc, %steatros l where lc.idteatro=l.idteatro and lc.fecha > '%s' group by l.localidad union all
        select count(*) as cont, localidad from %scines_has_peliculas lc, %scines l where lc.idcine=l.idcine and lc.fecha > '%s' group by l.localidad union all
        select count(*) as cont, localidad from %smuseos_has_peliculas lc, %smuseos l where lc.idmuseo=l.idmuseo and lc.fecha > '%s' group by l.localidad union all

        select count(*) as cont, localidad from %slocales_has_obrasteatro lc, %slocales l where lc.idlocal=l.idlocal and lc.fecha > '%s' group by l.localidad union all
        select count(*) as cont, localidad from %steatros_has_obrasteatro lc, %steatros l where lc.idteatro=l.idteatro and lc.fecha > '%s' group by l.localidad union all
        select count(*) as cont, localidad from %scines_has_obrasteatro lc, %scines l where lc.idcine=l.idcine and lc.fecha > '%s' group by l.localidad union all
        select count(*) as cont, localidad from %smuseos_has_obrasteatro lc, %smuseos l where lc.idmuseo=l.idmuseo and lc.fecha > '%s' group by l.localidad union all

        select count(*) as cont, localidad from %slocales_has_eventos lc, %slocales l where lc.idlocal=l.idlocal and lc.fecha > '%s' group by l.localidad union all
        select count(*) as cont, localidad from %steatros_has_eventos lc, %steatros l where lc.idteatro=l.idteatro and lc.fecha > '%s' group by l.localidad union all
        select count(*) as cont, localidad from %scines_has_eventos lc, %scines l where lc.idcine=l.idcine and lc.fecha > '%s' group by l.localidad union all
        select count(*) as cont, localidad from %smuseos_has_eventos lc, %smuseos l where lc.idmuseo=l.idmuseo and lc.fecha > '%s' group by l.localidad union all

        select count(*) as cont, localidad from %slocales_has_deportes lc, %slocales l where lc.idlocal=l.idlocal and lc.fecha > '%s' group by l.localidad union all
        select count(*) as cont, localidad from %steatros_has_deportes lc, %steatros l where lc.idteatro=l.idteatro and lc.fecha > '%s' group by l.localidad union all
        select count(*) as cont, localidad from %scines_has_deportes lc, %scines l where lc.idcine=l.idcine and lc.fecha > '%s' group by l.localidad union all
        select count(*) as cont, localidad from %smuseos_has_deportes lc, %smuseos l where lc.idmuseo=l.idmuseo and lc.fecha > '%s' group by l.localidad union all
        select count(*) as cont, localidad from %spabellones_has_deportes lc, %spabellones l where lc.idpabellon=l.idpabellon and lc.fecha > '%s' group by l.localidad union all

        select count(*) as cont, localidad from %slocales_has_cursos lc, %slocales l where lc.idlocal=l.idlocal and lc.fecha > '%s' group by l.localidad union all
        select count(*) as cont, localidad from %steatros_has_cursos lc, %steatros l where lc.idteatro=l.idteatro and lc.fecha > '%s' group by l.localidad union all
        select count(*) as cont, localidad from %scines_has_cursos lc, %scines l where lc.idcine=l.idcine and lc.fecha > '%s' group by l.localidad union all
        select count(*) as cont, localidad from %smuseos_has_cursos lc, %smuseos l where lc.idmuseo=l.idmuseo and lc.fecha > '%s' group by l.localidad union all

        select count(*) as cont, localidad from %slocales_has_exposiciones lc, %slocales l where lc.idlocal=l.idlocal and lc.fin > '%s' group by l.localidad union all
        select count(*) as cont, localidad from %steatros_has_exposiciones lc, %steatros l where lc.idteatro=l.idteatro and lc.fin > '%s' group by l.localidad union all
        select count(*) as cont, localidad from %smuseos_has_exposiciones lc, %smuseos l where lc.idmuseo=l.idmuseo and lc.fin > '%s' group by l.localidad

        ) as consulta group by localidad
        order by localidad",
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"]);
        //echo $query."</br>";
        //logFile($query);
        $r = $this->db->query($query);

        $result = array();
        while($localidades = $this->db->fetch($r)) {
            $result[] = $localidades;
        }

        return $result;
    }

    /**
     * Coge todos los eventos del dia actual para mostrarlos por pantalla y poder compartirlos en las redes sociales
     *
     * @param array $options
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listShareLinksHoy($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "and ".$options["filter"];
        }
        $between = explode(" ",$options["filter"]);
        $now = date("Y-m-d", strtotime("now"))." 23:59:59";
        if(date("G", strtotime("now")) > 5) {
            $expoFilter = "((inicio <= ".$between[3]."' and fin >= ".$between[3]."') or permanente = 1) and WEEKDAY(".$between[3]."')+1 not in (select cerrado from horarios where tipolugar='museo' and idlugar=me.idmuseo)";
            if(strpos($options["filter"],"infantil")) {
                $expoFilter .= " and infantil = 1";
            }
        } else {
            $expoFilter = "0";
        }
        $query = sprintf("select id, nombre, genero, fecha, infantil, localidad, tipoevento, shortUrl, todoeldia from (

        select c.idconcierto as id, nombre, genero, lc.fecha, infantil, localidad, 'conciertos' as tipoevento, shortUrl, todoeldia from %sconciertos c, (select idconcierto, fecha, localidad, todoeldia from %slocales_has_conciertos lc, %slocales l where lc.idlocal=l.idlocal and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.idconcierto = c.idconcierto %s union
        select c.idconcierto, nombre, genero, lc.fecha, infantil, localidad, 'conciertos', shortUrl, todoeldia from %sconciertos c, (select idconcierto, fecha, localidad, todoeldia from %steatros_has_conciertos lc, %steatros l where lc.idteatro=l.idteatro and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.idconcierto = c.idconcierto %s union
        select c.idconcierto, nombre, genero, lc.fecha, infantil, localidad, 'conciertos', shortUrl, todoeldia from %sconciertos c, (select idconcierto, fecha, localidad, todoeldia from %scines_has_conciertos lc, %scines l where lc.idcine=l.idcine and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.idconcierto = c.idconcierto %s union
        select c.idconcierto, nombre, genero, lc.fecha, infantil, localidad, 'conciertos', shortUrl, todoeldia from %sconciertos c, (select idconcierto, fecha, localidad, todoeldia from %smuseos_has_conciertos lc, %smuseos l where lc.idmuseo=l.idmuseo and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.idconcierto = c.idconcierto %s union
        select c.idconcierto, nombre, genero, tc.fecha, infantil, localidad, 'conciertos', shortUrl, todoeldia from %sconciertos c, (select idconcierto, fecha, localidad, todoeldia from %slugares_has_conciertos where agotado = 0 and cancelado = 0 order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s union

        select c.idpelicula, nombre, genero, lc.fecha, infantil, localidad, 'peliculas', shortUrl, todoeldia from %speliculas c, (select idpelicula, fecha, localidad, todoeldia from %slocales_has_peliculas lc, %slocales l where lc.idlocal=l.idlocal and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.idpelicula = c.idpelicula %s union
        select c.idpelicula, nombre, genero, lc.fecha, infantil, localidad, 'peliculas', shortUrl, todoeldia from %speliculas c, (select idpelicula, fecha, localidad, todoeldia from %steatros_has_peliculas lc, %steatros l where lc.idteatro=l.idteatro and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.idpelicula = c.idpelicula %s union
        select c.idpelicula, nombre, genero, lc.fecha, infantil, localidad, 'peliculas', shortUrl, todoeldia from %speliculas c, (select idpelicula, fecha, localidad, todoeldia from %smuseos_has_peliculas lc, %smuseos l where lc.idmuseo=l.idmuseo and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.idpelicula = c.idpelicula %s union
        select c.idpelicula, nombre, genero, tc.fecha, infantil, localidad, 'peliculas', shortUrl, todoeldia from %speliculas c, (select idpelicula, fecha, localidad, todoeldia from %slugares_has_peliculas where agotado = 0 and cancelado = 0 order by fecha asc) as tc where tc.idpelicula = c.idpelicula %s union

        select c.idobrateatro, nombre, genero, lc.fecha, infantil, localidad, 'obrasteatro', shortUrl, todoeldia from %sobrasteatro c, (select idobrateatro, fecha, localidad, todoeldia from %slocales_has_obrasteatro lc, %slocales l where lc.idlocal=l.idlocal and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.idobrateatro = c.idobrateatro %s union
        select c.idobrateatro, nombre, genero, lc.fecha, infantil, localidad, 'obrasteatro', shortUrl, todoeldia from %sobrasteatro c, (select idobrateatro, fecha, localidad, todoeldia from %steatros_has_obrasteatro lc, %steatros l where lc.idteatro=l.idteatro and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.idobrateatro = c.idobrateatro %s union
        select c.idobrateatro, nombre, genero, lc.fecha, infantil, localidad, 'obrasteatro', shortUrl, todoeldia from %sobrasteatro c, (select idobrateatro, fecha, localidad, todoeldia from %scines_has_obrasteatro lc, %scines l where lc.idcine=l.idcine and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.idobrateatro = c.idobrateatro %s union
        select c.idobrateatro, nombre, genero, lc.fecha, infantil, localidad, 'obrasteatro', shortUrl, todoeldia from %sobrasteatro c, (select idobrateatro, fecha, localidad, todoeldia from %smuseos_has_obrasteatro lc, %smuseos l where lc.idmuseo=l.idmuseo and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.idobrateatro = c.idobrateatro %s union
        select c.idobrateatro, nombre, genero, tc.fecha, infantil, localidad, 'obrasteatro', shortUrl, todoeldia from %sobrasteatro c, (select idobrateatro, fecha, localidad, todoeldia from %slugares_has_obrasteatro where agotado = 0 and cancelado = 0 order by fecha asc) as tc where tc.idobrateatro = c.idobrateatro %s union

        select c.ideventos, nombre, tipo, lc.fecha, infantil, localidad, 'eventos', shortUrl, todoeldia from %seventos c, (select ideventos, fecha, localidad, todoeldia from %slocales_has_eventos lc, %slocales l where lc.idlocal=l.idlocal and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.ideventos = c.ideventos %s union
        select c.ideventos, nombre, tipo, lc.fecha, infantil, localidad, 'eventos', shortUrl, todoeldia from %seventos c, (select ideventos, fecha, localidad, todoeldia from %steatros_has_eventos lc, %steatros l where lc.idteatro=l.idteatro and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.ideventos = c.ideventos %s union
        select c.ideventos, nombre, tipo, lc.fecha, infantil, localidad, 'eventos', shortUrl, todoeldia from %seventos c, (select ideventos, fecha, localidad, todoeldia from %scines_has_eventos lc, %scines l where lc.idcine=l.idcine and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.ideventos = c.ideventos %s union
        select c.ideventos, nombre, tipo, lc.fecha, infantil, localidad, 'eventos', shortUrl, todoeldia from %seventos c, (select ideventos, fecha, localidad, todoeldia from %smuseos_has_eventos lc, %smuseos l where lc.idmuseo=l.idmuseo and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.ideventos = c.ideventos %s union
        select c.ideventos, nombre, tipo, tc.fecha, infantil, localidad, 'eventos', shortUrl, todoeldia from %seventos c, (select ideventos, fecha, localidad, todoeldia from %slugares_has_eventos where agotado = 0 and cancelado = 0 order by fecha asc) as tc where tc.ideventos = c.ideventos %s union

        select c.iddeporte, c.nombre, td.idtipodeporte as tipo, lc.fecha, infantil, localidad, 'deportes', shortUrl, todoeldia from %sdeportes c LEFT JOIN %stipos_deporte td on td.idtipodeporte = c.tipo, (select iddeporte, fecha, localidad, todoeldia from %slocales_has_deportes lc, %slocales l where lc.idlocal=l.idlocal and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.iddeporte = c.iddeporte %s union
        select c.iddeporte, c.nombre, td.idtipodeporte as tipo, lc.fecha, infantil, localidad, 'deportes', shortUrl, todoeldia from %sdeportes c LEFT JOIN %stipos_deporte td on td.idtipodeporte = c.tipo, (select iddeporte, fecha, localidad, todoeldia from %steatros_has_deportes lc, %steatros l where lc.idteatro=l.idteatro and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.iddeporte = c.iddeporte %s union
        select c.iddeporte, c.nombre, td.idtipodeporte as tipo, lc.fecha, infantil, localidad, 'deportes', shortUrl, todoeldia from %sdeportes c LEFT JOIN %stipos_deporte td on td.idtipodeporte = c.tipo, (select iddeporte, fecha, localidad, todoeldia from %scines_has_deportes lc, %scines l where lc.idcine=l.idcine and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.iddeporte = c.iddeporte %s union
        select c.iddeporte, c.nombre, td.idtipodeporte as tipo, lc.fecha, infantil, localidad, 'deportes', shortUrl, todoeldia from %sdeportes c LEFT JOIN %stipos_deporte td on td.idtipodeporte = c.tipo, (select iddeporte, fecha, localidad, todoeldia from %smuseos_has_deportes lc, %smuseos l where lc.idmuseo=l.idmuseo and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.iddeporte = c.iddeporte %s union
        select c.iddeporte, c.nombre, td.idtipodeporte as tipo, lc.fecha, infantil, localidad, 'deportes', shortUrl, todoeldia from %sdeportes c LEFT JOIN %stipos_deporte td on td.idtipodeporte = c.tipo, (select iddeporte, fecha, localidad, todoeldia from %spabellones_has_deportes lc, %spabellones l where lc.idpabellon=l.idpabellon and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.iddeporte = c.iddeporte %s union
        select c.iddeporte, c.nombre, td.idtipodeporte as tipo, tc.fecha, infantil, localidad, 'deportes', shortUrl, todoeldia from %sdeportes c LEFT JOIN %stipos_deporte td on td.idtipodeporte = c.tipo, (select iddeporte, fecha, localidad, todoeldia from %slugares_has_deportes where agotado = 0 and cancelado = 0 order by fecha asc) as tc where tc.iddeporte = c.iddeporte %s union

        select c.idcurso, nombre, tipo, lc.fecha, infantil, localidad, 'formacion', shortUrl, todoeldia from %scursos c, (select idcurso, fecha, localidad, todoeldia from %slocales_has_cursos lc, %slocales l where lc.idlocal=l.idlocal and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.idcurso = c.idcurso %s union
        select c.idcurso, nombre, tipo, lc.fecha, infantil, localidad, 'formacion', shortUrl, todoeldia from %scursos c, (select idcurso, fecha, localidad, todoeldia from %steatros_has_cursos lc, %steatros l where lc.idteatro=l.idteatro and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.idcurso = c.idcurso %s union
        select c.idcurso, nombre, tipo, lc.fecha, infantil, localidad, 'formacion', shortUrl, todoeldia from %scursos c, (select idcurso, fecha, localidad, todoeldia from %scines_has_cursos lc, %scines l where lc.idcine=l.idcine and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.idcurso = c.idcurso %s union
        select c.idcurso, nombre, tipo, lc.fecha, infantil, localidad, 'formacion', shortUrl, todoeldia from %scursos c, (select idcurso, fecha, localidad, todoeldia from %slocales_has_cursos lc, %slocales l where lc.idlocal=l.idlocal and agotado = 0 and cancelado = 0 order by fecha asc) as lc where lc.idcurso = c.idcurso %s union
        select c.idcurso, nombre, tipo, tc.fecha, infantil, localidad, 'formacion', shortUrl, todoeldia from %scursos c, (select idcurso, fecha, localidad, todoeldia from %slugares_has_cursos where agotado = 0 and cancelado = 0 order by fecha asc) as tc where tc.idcurso = c.idcurso %s union

        select e.idexposicion, e.nombre, genero, '".$now."', infantil, localidad, 'exposiciones', shortUrl, '1' from %sexposiciones e, %smuseos_has_exposiciones me, %smuseos m where me.idexposicion = e.idexposicion and m.idmuseo = me.idmuseo and date(inicio) = date('".$now."') and %s and e.publicado = 1 union
        select e.idexposicion, e.nombre, genero, '".$now."', infantil, localidad, 'exposiciones', shortUrl, '1' from %sexposiciones e, %slocales_has_exposiciones me, %slocales m where me.idexposicion = e.idexposicion and m.idlocal = me.idlocal and date(inicio) = date('".$now."') and %s and e.publicado = 1 union
        select e.idexposicion, e.nombre, genero, '".$now."', infantil, localidad, 'exposiciones', shortUrl, '1' from %sexposiciones e, %steatros_has_exposiciones me, %steatros m where me.idexposicion = e.idexposicion and m.idteatro = me.idteatro and date(inicio) = date('".$now."') and %s and e.publicado = 1 union
        select e.idexposicion, e.nombre, genero, '".$now."', infantil, localidad, 'exposiciones', shortUrl, '1' from %sexposiciones e, %slugares_has_exposiciones me where me.idexposicion = e.idexposicion and %s and e.publicado = 1 and date(inicio) = date('".$now."')

        ) as consulta
        order by %s %s",
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, $options["filter"],
        BDPREFIX, BDPREFIX, BDPREFIX, $expoFilter,
        BDPREFIX, BDPREFIX, BDPREFIX, str_replace("idmuseo", "idlocal", $expoFilter),
        BDPREFIX, BDPREFIX, BDPREFIX, str_replace("idmuseo", "idteatro", $expoFilter),
        BDPREFIX, BDPREFIX, str_replace(" and idlugar=me.idmuseo", "", $expoFilter),
        $this->db->secure_field($options["order"]),$this->db->secure_field($limit));
        //logFile($query);
        $r = $this->db->query($query);

        $result = array();
        while($pelicula = $this->db->fetch($r)) {
            $result[] = $pelicula;
        }

        return $result;
    }

    /**
     * Coge todos los horarios de apertura de un sitio
     *
     * @param array $options
     * @param int $options["idlugar"] id del lugar seleccionado
     * @param string $options["tipolugar"] tipo del lugar seleccionado (museo, local, cine...)
     * @return array
     */
    public function listHorarios($options) {
        $query = sprintf("select h.* from %shorarios h where h.idlugar = %d and h.tipolugar = '%s'", BDPREFIX, $this->db->secure_field($options["idlugar"]), $this->db->secure_field($options["tipolugar"]));
        $r = $this->db->query($query);

        $result = array();
        while($horarios = $this->db->fetch($r)) {
            $result[] = $horarios;
        }

        return $result;
    }

    /*
     * Eliminar un horario de apertura
     *
     * @param array $options
     * @param int $options["idlugar"] id del lugar a eliminar
     * @param string $options["tipolugar"] tipo del lugar a eliminar (museo, local, cine...)
     * @return boolean
     */
    public function deleteHorario($options) {
        if(!empty($options["idlugar"]) && !empty($options["tipolugar"])) {
            $query = sprintf("delete from %shorarios where idlugar = %d and tipolugar = '%s'",BDPREFIX, $this->db->secure_field($options["idlugar"]), $this->db->secure_field($options["tipolugar"]));
            $r = $this->db->execute($query);
            return true;
        }
        return false;
    }

    /*
     * Insertar un horario de apertura
     *
     * @param array $horario datos del horario
     * @param int $horario['idlugar'] id del lugar que tiene este horario
     * @param string $horario['tipolugar'] tipo del lugar que tiene este horario (museo, local, cine...)
     * @param string $horario['lunes']
     * @param string $horario['martes']
     * @param string $horario['miercoles']
     * @param string $horario['jueves']
     * @param string $horario['viernes']
     * @param string $horario['sabado']
     * @param string $horario['domingo']
     * @param string $horario['cerrado']
     * @param string $horario['cerradosueltos']
     * @return int
     */
    public function addHorario($horario) {
        if(!$this->checkHorario($horario)) {
            $fields = "";
            $values = "";
            foreach($horario as $key => $value) {
                if(!empty($fields)) {
                    $fields = $fields.",";
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $query = sprintf("insert into %shorarios (%s) VALUES (%s)", BDPREFIX, $fields, $values);
            $r = $this->db->execute($query);
            if($r) {
                $id = $this->db->last_id();
                return $id;
            } else {
                throw new Exception("[addHorario] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[addHorario] Ya existe el horario.", 1);
        }
    }

    /*
     * Actualiza los campos de una horario de apertura
     *
     * @param array $horario datos del horario
     * @param int $horario['idlugar'] id del lugar que tiene este horario
     * @param string $horario['tipolugar'] tipo del lugar que tiene este horario (museo, local, cine...)
     * @param string $horario['lunes']
     * @param string $horario['martes']
     * @param string $horario['miercoles']
     * @param string $horario['jueves']
     * @param string $horario['viernes']
     * @param string $horario['sabado']
     * @param string $horario['domingo']
     * @param string $horario['cerrado']
     * @param string $horario['cerradosueltos']
     * @return boolean
     */
    public function updateHorario($horario) {
        if($this->checkHorario($horario)) {
            $fields = "";
            $exclude = array("idlugar","idhorario","tipolugar");
            foreach($horario as $key => $value) {
                if(!in_array($key, $exclude)) {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $query = sprintf("update %shorarios set %s where idlugar = %d and tipolugar = '%s'", BDPREFIX, $fields, $this->db->secure_field($horario["idlugar"]), $this->db->secure_field($horario["tipolugar"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateHorario] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateHorario] No existe el horario.", 1);
        }
    }

    /*
     * Comprueba si existe el horario de apertura
     *
     * @param array $options
     * @param string $options["idlugar"] id del lugar
     * @param string $options["tipolugar"] tipo del lugar (museo, local, cine...)
     * @return int|false
     */
    private function checkHorario($options) {
        $query = sprintf("select h.idlugar from %shorarios h where h.idlugar = %d and h.tipolugar = '%s'", BDPREFIX, $this->db->secure_field($options["idlugar"]), $this->db->secure_field($options["tipolugar"]));
        $r = $this->db->query($query);
        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }

    /*
     * Eliminar una imagen de un formulario
     *
     * @param int $id id al que pertenece el elemento
     * @param string $path ruta de la imagen
     * @return boolean
     */
    public function deleteImage($id,$path) {
        if(!empty($id) && !empty($path)) {
            if(strpos($path,"poster") != false) {
                $field = "poster";
            } elseif(strpos($path,"fanart") != false) {
                $field = "fanart";
            } else {
                $field = "imagen";
            }
            $img = explode("/",$path);
            switch($img[1]) {
                case "cines":
                    $table = "cines";
                    $idfield = "idcine";
                    break;
                case "conciertos":
                    $table = "conciertos";
                    $idfield = "idconcierto";
                    break;
                case "cursos":
                    $table = "cursos";
                    $idfield = "idcurso";
                    break;
                case "deportes":
                    $table = "deportes";
                    $idfield = "iddeporte";
                    break;
                case "entradas":
                    $table = "entradas";
                    $idfield = "identradas";
                    break;
                case "eventos":
                    $table = "eventos";
                    $idfield = "idevento";
                    break;
                case "expos":
                    $table = "exposiciones";
                    $idfield = "idexposicion";
                    break;
                case "locales":
                    $table = "locales";
                    $idfield = "idlocal";
                    break;
                case "museos":
                    $table = "museos";
                    $idfield = "idmuseo";
                    break;
                case "obrasteatro":
                    $table = "obrasteatro";
                    $idfield = "idobrateatro";
                    break;
                case "pabellones":
                    $table = "pabellones";
                    $idfield = "idpabellon";
                    break;
                case "peliculas":
                    $table = "peliculas";
                    $idfield = "idpelicula";
                    break;
                case "promos":
                    $table = "promociones";
                    $idfield = "idpromocion";
                    break;
                case "teatros":
                    $table = "teatros";
                    $idfield = "idteatro";
                    break;
            }
            $query = sprintf("update %s set %s = '' where %s = %d and %s = '%s'",BDPREFIX.$table, $field, $idfield, $this->db->secure_field($id), $field, $this->db->secure_field($path));
            $r = $this->db->execute($query);
            if($r) {
                //Eliminamos imagen
                if(!unlink(BASE_URI.$path)) {
                    return false;
                }
                //Si es poster eliminamos la miniatura
                if($field == "poster") {
                    $img[4] = $img[3];
                    $img[3] = "thumbs";
                    $path = implode("/",$img);
                    if(!unlink(BASE_URI.$path)) {
                        return false;
                    }
                }
                //Si es poster o imagen eliminamos el original
                if($field != "fanart") {
                    $img[4] = $img[3];
                    $img[3] = "original";
                    $path = implode("/",$img);
                    if(!unlink(BASE_URI.$path)) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }
}
// END
