    <footer>
        <div id='footer-social'>
            <a href='https://play.google.com/store/apps/details?id=com.hazplanes.hazplanes' target='_blank'><img src='img/interface/logops-29.png' alt='Logo Google Play Store' width='29' height='29'/></a>
            <a href='https://plus.google.com/+HazplanesWeb?rel=author' target='_blank'><img src='img/interface/logogp-29.png' alt='Logo Google+' width='29' height='29'/></a>
            <a href='https://www.facebook.com/hazplanesweb' target='_blank'><img src='img/interface/logofb-29.png' alt='Logo Facebook' width='29' height='29'/></a>
            <a href='https://twitter.com/HazPlanes' target='_blank'><img src='img/interface/logotw-29.png' alt='Logo Twitter' width='29' height='29'/></a>
        </div>
    </footer>
</body>
</html>
