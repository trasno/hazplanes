<?php
// Check for a $page_title value:
if (!isset($page_title)) $page_title = 'HazPlanes';

//Filtro de Fecha
if(isset($_GET["d"]) && !empty($_GET["d"])) {
    $filtroFecha = date("d-m-Y",strtotime($_GET["d"]));
    $menuTit = $filtroFecha;
} else {
    $filtroFecha = "+0";
}
$nofecha = false;
$displayContent = "";
if(isset($_COOKIE["display"])) {
    if($_COOKIE["display"] == 1 || $_COOKIE["display"] == 2) {
        $displayContent = "lista";
        $display = "linea";
        if($_COOKIE["display"] == 2) {
            $nofecha = true;
        }
    } else {
        $display = "celda";
    }
} else {
    $display = "celda";
}
if(isset($_COOKIE["density"])) {
    if($_COOKIE["density"] == 1) {
        $display .= " density-compact";
    } else {
        $display .= " density-normal";
    }
} else {
    $display .= " density-normal";
}
if($p == "novedades" || $p == "conreserva") {
    $display = "linea";
}
$celdaPubli = "";
$publiMenu = "";
$adsScript = "";
$publiFicha = "";

//Bloque rectangulo 300x250
$celdaPubli = "<li class='publicidad all ".$display."'>
                        <!-- Bloque de HazPlanes cuadrado -->
                        <ins class='adsbygoogle'
                             style='display:inline-block;width:300px;height:250px'
                             data-ad-client='ca-pub-0179166557701407'
                             data-ad-slot='6237032543'></ins>
                        <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </li>";

//Bloque cuadrado 250x250
/*$celdaPubli = "<li class='publicidad all ".$display."'>
                        <!-- Bloque de HazPlanes cuadrado -->
                        <ins class='adsbygoogle'
                             style='display:inline-block;width:250px;height:250px'
                             data-ad-client='ca-pub-0179166557701407'
                             data-ad-slot='3504870148'></ins>
                        <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </li>";*/
$celdaPubliAdaptable = "<li class='publicidad all ".$display."'>
                        <!-- HazPlanes Adaptable -->
                        <ins class='adsbygoogle'
                             style='display:block;'
                             data-ad-client='ca-pub-0179166557701407'
                             data-ad-slot='7662746542'
                             data-ad-format='auto''></ins>
                         <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </li>";
if(strpos($display,"celda") === false) {
    $celdaPubli = $celdaPubliAdaptable;
}
$publiMenu = '<span class="publiMenu">
                    <!-- HazPlanes menu lateral -->
                    <ins class="adsbygoogle"
                         style="display:inline-block;width:200px;height:60px"
                         data-ad-client="ca-pub-0179166557701407"
                         data-ad-slot="9223549349"></ins>
                    <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </span>';
$publiFicha = '<span class="publiMenu">
                    <!-- HazPlanes Ficha -->
                    <ins class="adsbygoogle"
                         style="display:inline-block;width:320px;height:100px"
                         data-ad-client="ca-pub-0179166557701407"
                         data-ad-slot="6373068147"></ins>
                    <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </span>';
if(!$local) {
    $adsScript = '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>';
}
?>
<!doctype html>
<html prefix="og: http://ogp.me/ns#" lang="es"> <?php // manifest="http://hazplanes.com/hazplanes.appcache" ?>
<head>
    <base href="<?php echo BASE_URL; ?>" />
    <meta charset="utf-8" />
    <?php
    if(isset($ogtitle)) {
        /*if(strlen($ogdescription) < 120) {
            $ogdescription .= " - Hazplanes - Agenda de ocio, eventos, conciertos, exposiciones, cartelera y mucho más en la ciudad de Vigo y alrededores.";
        }*/
        $ogdescription = substr(strip_tags($ogdescription),0,150);
    ?>
    <meta property="og:title" content="<?php echo $ogtitle; ?>" />
    <meta property="og:url" content="<?php echo $ogurl; ?>" />
    <meta property="og:description" content="<?php echo $ogdescription; ?>" />
    <meta property="og:image" content="<?php echo $ogimage; ?>" />
    <meta property="og:site_name" content="HazPlanes">
    <meta property="fb:app_id" content="1496972530518484" />
    <meta itemprop="name" content="<?php echo $ogtitle; ?>">
    <meta itemprop="description" content="<?php echo $ogdescription; ?>">
    <meta itemprop="image" content="<?php echo $ogimage; ?>">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:image" content="<?php echo $ogimage; ?>">
    <meta name="twitter:title" content="<?php echo $ogtitle; ?>">
    <meta name="twitter:description" content="<?php echo $ogdescription; ?>">
    <meta name="twitter:site" content="@hazplanes">
    <meta name="twitter:creator" content="@hazplanes">
    <meta name="twitter:domain" content="HazPlanes">
    <meta name="description" content="<?php echo $ogdescription; ?>" />
    <?php } else { ?>
    <meta name="description" content="Agenda de ocio, eventos, conciertos, exposiciones, cartelera y mucho más en la ciudad de Vigo y alrededores. <?php echo ucwords($p); ?>" />
    <?php } ?>
    <meta name="author" content="AperCloud" />
    <meta name="keywords" content="Vigo, Eventos, Agenda, Ocio, Conciertos, Cartelera, Exposiciones, Deportes, Teatro, Auditorios">
    <meta name="robots" content="index, follow">
    <meta name="viewport" content="initial-scale=1.0, width=device-width" />

    <title><?php echo $page_title; ?></title>

    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="styles/styles.css?v1" />
    <link href='http://fonts.googleapis.com/css?family=Roboto&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.10.4.min.js"></script>
    <?php //<script type="text/javascript" src="js/isotope.pkgd.min.js"></script> ?>
    <script>fecha = '<?php echo $filtroFecha; ?>';</script>
    <script async type="text/javascript" src="js/main.js"></script>
    <?php
    $pagesWithMaps = array("cine","local","auditorio","museo","competicion","curso","evento","pabellon","concierto","pelicula","exposicion","obra","mainmap");
    if(in_array($p,$pagesWithMaps)) {
        ?>
        <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDatF3wwcD0PS5IIKl4E1t8XQVpfEfbiQs&sensor=false"></script>-->
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
        <script type="text/javascript" src="js/maps.js"></script>
        <?php
    }
    ?>
</head>
<body <?php if(isset($_COOKIE["pin"]) && $_COOKIE["pin"] == 1) echo "class='pml-open'"; ?>>
    <?php if(isset($id) && !empty($id)) { ?>
        <div class='ampliarCapa'>
            <div class='ampliarCapa-bg'></div>
            <div class='ampliarCapa-msg' title='Cerrar'>
                <img src='' id='ampliarImg' alt='Imagen ampliada' />
            </div>
        </div>
    <?php } ?>
    <?php echo $adsScript; ?>
    <?php include("menulateral.php") ?>
    <?php include("menuheader.php") ?>
	<!-- End of header. -->
