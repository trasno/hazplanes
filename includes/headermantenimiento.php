<?php
// Check for a $page_title value:
if (!isset($page_title)) $page_title = PAGENAME;

?>
<!doctype html>
<html lang="es" manifest="http://hazplanes.com/hazplanes.appcache">
<head>
	<base href="<?php echo BASE_URL; ?>" />
    <meta charset="utf-8" />
    <meta name="author" content="Aper" />
    <meta name="description" content="Agenda de eventos a los que asistir en la ciudad de Vigo y alrededores." />
    <meta name="keywords" content="Vigo, Eventos, Agenda, Ocio, Conciertos, Cartelera, Exposiciones, Deportes, Teatro, Auditorios">
    <meta name="robots" content="index, follow">
    <meta name="viewport" content="initial-scale=1.0, width=device-width" />

    <title><?php echo $page_title; ?></title>

    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="styles/styles.css" />
    <link rel="stylesheet" type="text/css" href="styles/mantenimiento.css" />
    <link href='http://fonts.googleapis.com/css?family=Roboto&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>
<body>
    <nav id="filters">
        <span id='header-logo'>
            <a href='main'>
                <?php echo PAGENAME; ?> <span style='font-size: 9px;'><?php echo VERSION; ?></span>
            </a>
        </span>
        <div id='sectionTitle'><?php echo $page_title; ?></div>
    </nav>
	<!-- End of header. -->
