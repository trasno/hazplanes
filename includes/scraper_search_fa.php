<?php
include_once("api.inc.php");
include_once("scrapers.inc.php");

$debug = "";

if (isset($_POST['titulo'])) { // Forms
    $titulo = $_POST['titulo'];
} else {
    $titulo = "";
}

if(!empty($titulo)) {
    //Recoger informacion de la pelicula en filmaffinity
    $title = str_replace("á","a",str_replace("é","e",str_replace("í","i",str_replace("ó","o",str_replace("ú","u",strtolower($titulo))))));
    $anho = date("Y",strtotime("now"));
    $title = str_replace(" ","+",$title);

    //Sensacine
    $distrib = "";
    $url = "http://www.sensacine.com/busqueda/1/?q=".$title;
    $search = file_get_contents_curl($url);
    if($search !== false && !empty($search)) {
        preg_match_all('/<a href=\'\/peliculas\/pelicula-(.*?)\//s',$search,$sensacineID);
        if(isset($sensacineID[1][0]) && !empty($sensacineID[1][0])) {
            $url = "http://www.sensacine.com/peliculas/pelicula-".$sensacineID[1][0];
            $search = file_get_contents_curl($url);
            if($search !== false) {
                preg_match_all('/itemprop=\"productionCompany\">(.*?)<\/span>/s',$search,$distrib);
            }
        }
    }

    //FilmAffinity
    if(strpos($title,"ñ") || strpos($title,"Ñ")) {
        $pos = strpos($title,"ñ");
        if(!$pos) {
            $pos = strpos($title,"Ñ");
        }
        $title = substr($title,0,$pos-1);
    }
    $url = "http://www.filmaffinity.com/es/advsearch.php?stext=".$title;
    $search = file_get_contents_curl($url);
    if($search !== false) {
        preg_match_all('/href=\"\/es\/film(.*?)\.html/s',$search,$filmaffinityID);
        $total = count($filmaffinityID[1]);
        $peliDB = array();
        $cont = 0;
        for($k = 1; $k < $total; $k += 2) {
            if(isset($filmaffinityID[1][$k])) {
                $url = "http://www.filmaffinity.com/es/film".$filmaffinityID[1][$k].".html";
                $peliDB[$cont]["ficha"] = $url;
                $peliDB[$cont]["fichafa"] = $url;
                $search = file_get_contents_curl($url);
                preg_match_all('/<span itemprop=\"name\">(.*?)<\/span>/s',$search,$tit);
                preg_match_all('/<dt>(.*?)<\/dt>/s',$search,$keys);
                preg_match_all('/<dd(.*?)<\/dd>/s',$search,$values);
                $info = array();
                $num = count($values[1]);
                //echo json_encode($keys);
                for($i = 0; $i < $num; $i++) {
                    if(isset($values[1][$i]) && isset($keys[1][$i])) {
                        $key = htmlspecialchars($keys[1][$i]);
                        $info[strtolower($key)] = strip_tags(trim(ltrim($values[1][$i],">")));
                    }
                }
                if(isset($tit[1][0])) {
                    $peliDB[$cont]["nombre"] = strip_tags(trim($tit[1][0]));
                }
                if(isset($info["título original"])) {
                    $peliDB[$cont]["original"] = $info["título original"];
                }
                if(isset($anhos) && isset($anhos[1][0])) {
                    $peliDB[$cont]["edad"] = $anhos[1][0];
                }
                if(isset($distrib) && isset($distrib[1][0])) {
                    $peliDB[$cont]["distribuidora"] = $distrib[1][0];
                }
                if(isset($info["duración"])) {
                    $duracion = explode(" ",$info["duración"]);
                    $peliDB[$cont]["duracion"] = (int) $duracion[0];
                }
                if(isset($info["país"])) {
                    $peliDB[$cont]["nacionalidad"] = str_replace("&nbsp;","",$info["país"]);
                }
                if(isset($info["director"])) {
                    $peliDB[$cont]["director"] = $info["director"];
                }
                if(isset($info["reparto"])) {
                    $peliDB[$cont]["actores"] = $info["reparto"];
                }
                if(isset($info["género"])) {
                    $genero = explode("|",$info["género"]);
                    $peliDB[$cont]["genero"] = str_replace("         ","",$genero[0]);
                }
                if(isset($info["web oficial"])) {
                    $peliDB[$cont]["web"] = str_replace("class=\"web-url\">","",$info["web oficial"]);
                }
                if(isset($info["sinopsis"])) {
                    $peliDB[$cont]["sinopsis"] = $info["sinopsis"];
                    //$debug[] = $info["sinopsis"];
                }
                $cont++;
            }
        }

        $msg = array("success" => "ok","data" => $peliDB, "debug" => $debug);
    } else {
        $msg = array("success" => "ko","data" => "Error al buscar", "debug" => $debug);
    }
} else {
    $msg = array("success" => "ko","data" => "Título vacío", "debug" => $debug);
}

echo json_encode($msg);
?>