api = "api/v2/";

$( function() {
	$('form').on("keypress",function(e) {
		if (e.keyCode == '13') {
			e.preventDefault();
		}
	});
	$("[name=submit]").on("click",function(event){
		var vals = new Array();
		$(".tabla-fechas input[type=datetime]").each(function() {
			if($.inArray($(this).val(), vals) == -1) { //Not found
		    	vals.push($(this).val());
		  	} else {
		    	alert("Fecha duplicada: " + $(this).val());
		    	event.preventDefault();
		  	}
		});
	});
    $("#delete").on("click",function() {
        if(confirm("¿Desea eliminar los elementos seleccionados?")) {
            var controller = $(this).attr("c");
            var ids = "";
	    	$(':checkbox[name=checkListItem]:checked').each(function() {
	    		$(this).attr('checked', false);
	    		$("#listOptions").slideUp("fast");
	    		var id = $(this).attr("id");
	    		$.ajax({
	                type: "POST",
	                url: api + "index.php",
	                data: { controller: controller, action: "Delete", id: id }
	            })
	            .done(function( msg ) {
	            	msg = JSON.parse(msg);
	            	console.log(msg);
	            	if(msg.success == true) {
	            		$("[id="+id+"]").parent().parent().fadeOut("slow", function() {
		                    $(this).remove();
	                	});
	               	} else {
	               		console.log(msg);
               		}
	            });
	    	});
	    	$("#listOptions").slideUp("fast");
        }
        return false;
    });

    $("#menulateral li").click(function(e){
    	e.preventDefault();
    	window.location.href = $("a",this).attr('href');
    });

    $('#filters a').hover(
        function() {
            var text = $(this).children("span").text();
            $("#sectionTitle").text(text);
        }, function() {
            var text = $("#filters .current").children().children("span").text();
            if(text == "") {
                text = "Inicio";
            }
            $("#sectionTitle").text(text);
        }
    );

    $(document).on("click",':checkbox[name=checkListItem]', function () {
    	if(this.checked) {
        	$("#listOptions").slideDown("fast");
        } else {
        	if($(':checkbox[name=checkListItem]:checked').length < 1) {
        		$("#listOptions").slideUp("fast");
        	}
        }
    });

    $(':checkbox[name=checkall]').click (function () {
        $(':checkbox[name=checkListItem]').prop('checked', this.checked);
        if(this.checked) {
        	$("#listOptions").slideDown("fast");
        } else {
        	$("#listOptions").slideUp("fast");
        }
    });

    $("#publish").on("click", function() {
    	var controller = $(this).attr("c");
    	var ids = "";
    	$(':checkbox[name=checkListItem]:checked').each(function() {
    		if($(this).parent().next().text() == "No") {
	    		if(ids != "") {
	    			ids += ",";
	    		}
	    		ids += $(this).attr("id");
	    	}
    	});
    	if(ids != "") {
	    	$.ajax({
	            type: "POST",
	            url: api + "index.php",
	            data: { controller: controller, action: "Update", ids: ids, op: "publish" }
	        })
	        .done(function( msg ) {
	        	msg = JSON.parse(msg);
	        	if(msg.success == true) {
		            id = ids.split(",");
		            for(i=0; i< id.length; i++) {
		            	$("[id="+id[i]+"]").parent().next().text("Si");
		            }
		            $(':checkbox[name=checkListItem]').prop('checked', false);
		            $("#listOptions").slideUp("fast");
	           	}
	        });
	        $(':checkbox[name=checkall]').prop('checked', false);
       }
    });
    $("#unpublish").on("click", function() {
    	var controller = $(this).attr("c");
    	var ids = "";
    	$(':checkbox[name=checkListItem]:checked').each(function() {
    		if($(this).parent().next().text() == "Si") {
	    		if(ids != "") {
	    			ids += ",";
	    		}
	    		ids += $(this).attr("id");
	    	}
    	});
    	if(ids != "") {
	    	$.ajax({
	            type: "POST",
	            url: api + "index.php",
	            data: { controller: controller, action: "Update", ids: ids, op: "unpublish" }
	        })
	        .done(function( msg ) {
	        	msg = JSON.parse(msg);
            	if(msg.success == true) {
            		id = ids.split(",");
	            	for(i=0; i< id.length; i++) {
		            	$("[id="+id[i]+"]").parent().next().text("No");
	            	}
	            	$(':checkbox[name=checkListItem]').prop('checked', false);
	            	$("#listOptions").slideUp("fast");
	            }
	        });
	        $(':checkbox[name=checkall]').prop('checked', false);
       }
    });

	$(document).on("click",'.setCoord', function () {
		var id = $(this).prev().attr("id");
        var val = $(this).prev().val();
		$("#coordenadasfield").val(id);
		$("#mapafondo").show();
		google.maps.event.trigger(map, 'resize');
		if(val != "") {
			$("#coordenadastemp").val(val);
			coord = val.replace("(","").replace(")","").split(",");
			setPoints(coord[0].replace(" ",""), coord[1].replace(" ",""));
			$("#puntosPoly").text(val);
		} else {
			map.setCenter(new google.maps.LatLng(42.2181, -8.725));
		}
    });

	$("#setCoord").on("click", function() {
		var id = $("#coordenadasfield").val();
		var val = $("#coordenadastemp").val();

		$("#"+id).val(val);
		$("#mapafondo").hide();
		marker.setMap(null);
    });

    $("#cleanCoord").on("click", function() {
		$("#coordenadastemp").val("");
		$("#puntosPoly").text("");
		marker.setMap(null);
    });

    jQuery.expr[':'].contains = function(a, i, m) {
	  	return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
	};

    $("#search-field").on("keyup", function(e) {
    	var code = e.which; // recommended to use e.which, it's normalized across browsers
    	if(code==13) {
    		e.preventDefault();

	    	var busca = $(this).val();
	    	var type = $("#type").val();
	    	if(busca.length > 1 || busca.length < 1) {
	    		$.ajax({
		            cache: false,
		            type: "POST",
		            url: './ajax/search.php',
		            data: { type: type, search: busca },
		            dataType: "json"
		        })
		        .done(function(response) {
		            if(response.status === 'ok') {
		            	if(busca == "") {
		            		$(".paginacion").show();
		            	} else {
		            		$(".paginacion").hide();
		            	}
		            	$("#listrows").html("");
						for(i=0; i<response.msg.length; i++) {
							$("#listrows").append(response.msg[i]);
						}
		            } else {
		            	console.log(response);
		            }
		        })
		        .fail(function(msg) {
		            console.log(msg);
		        });
	        }
        }
    });
});

//Eliminar una imagen
function deleteImage(obj, id, path, src) {
	if(confirm("¿Desea eliminar la imagen seleccionada?")) {
		$.ajax({
            type: 'POST',
            url: api + 'index.php',
            data: { controller: src, action: "Delete", id: id, image: path }
        })
        .done(function(msg) {
            $(obj).parent().slideUp();
        });
    }
};