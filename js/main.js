api = "api/v2/";
$(function() {
    // cache container
    var $container = $('.grid');
    if(getCookie("density") == 1) {
    	columnWidth = 145;
    } else {
    	columnWidth = 270;
    }
    var pagina = window.location.pathname.split("/");

    /*Menu lateral*/
    $("#menulateral li").click(function(e){
    	e.preventDefault();
    	window.location.href = $("a",this).attr('href');
    });
    $("#menutoggle").on("click", function(event){
		event.preventDefault();
		if($("#menulateral").css("left") == "-200px") {
			$("#menulateral").css("left","0");
		} else if($("#content").css("left") != "200px" && $("#contentMap").css("left") != "200px") {
			$("#menulateral").css("left","-200px");
		}
	});
	/*Fin menu lateral*/

    /*Menu Cabecera*/
   $("#view-density li").click(function(e){
    	if($(this).attr("id") == "density-normal") {
    		$("#celdas > li").removeClass("density-compact");
    		$("#celdas > li").addClass("density-normal");
    		setCookie("density","0",0);
    	} else if($(this).attr("id") == "density-compact") {
    		$("#celdas > li").removeClass("density-normal");
    		$("#celdas > li").addClass("density-compact");
    		setCookie("density","1",0);
    	}

    	$("#view-density li").each(function(){
    		$(this).removeClass("current");
    	});
    	$(this).addClass("current");
    });
    $("#view-style li").click(function(e){
    	if($(this).attr("id") == "card") {
    		if($("#celdas > li.publicidad iframe").css("width") != "250px") {
    			$(".publicidad").hide();
    		} else {
    			$(".publicidad").show();
    		}
    		$("#content").removeClass("lista");
    		$("#celdas > li").removeClass("linea");
    		$("#celdas > li").addClass("celda");
    		$(".fecha").show();
    		$(".type-title").hide();
    		setCookie("display","0",0);
    	} else if($(this).attr("id") == "list") {
    		if($("#celdas > li.publicidad iframe").css("width") == "250px") {
    			$(".publicidad").hide();
    		} else {
    			$(".publicidad").show();
    		}
    		$("#celdas > li").removeClass("celda");
    		$("#celdas > li").addClass("linea");
    		$("#content").addClass("lista");
    		$(".fecha").show();
    		$(".type-title").hide();
    	    setCookie("display","1",0);
    	} else if($(this).attr("id") == "type") {
    		if($("#celdas > li.publicidad iframe").css("width") == "250px") {
    			$(".publicidad").hide();
    		} else {
    			$(".publicidad").show();
    		}
    		$("#celdas > li").removeClass("celda");
    		$("#celdas > li").addClass("linea");
    		$("#content").addClass("lista");
    		$(".type-title").css("display","inline-block");
    		$(".fecha:not(:contains('En proceso'))").hide();
    		setCookie("display","2",0);
		} else if($(this).attr("id") == "map") {
			location.href = "mainmap";
    	}

    	$("#view-style li").each(function(){
    		$(this).removeClass("current");
    	});
    	$(this).addClass("current");
    });
    $("#view-filter input[type=checkbox]").on("click",function(){
    	$("#view-filter :checkbox").each(function(){
    		var filtro = ".fecha, .publicidad, .conreserva, .all, .type-title";
    		var clas = $(this).attr("id");
    		if(!$(this).is(":checked")) {
    			$("#main li."+clas).hide();
    			setCookie("filter-"+clas,"0",0);
    		} else {
    			setCookie("filter-"+clas,"1",0);
    			$("#main li."+clas).show();
    			$("#main li."+clas+".type-title").hide();
    		}
    	});
    });
    /*Dropdown*/
    $("#header-options li > img").click(function(){
    	$(this).parent().children(".dropdown-menu").toggle();
    	if($(this).parent().children(".dropdown-menu").css('display') != 'none') {
    		$(this).parent().addClass("current");
    	}
    });
    $("#header-options > li").on("mouseleave", function(){
    	$("#header-options > li").each(function(){
    		$(this).removeClass("current");
    		$(this).children(".dropdown-menu").hide();
    	});
    });
    /*Fin dropdown*/
    /*Fin Menu Cabecera*/

    $(".celda:not(.publicidad):not(.conreserva):not(.fecha):not(.noclick):not(.stamp)").on("click",function() {
        window.location.href = $(this).find('.grid-info a:first').attr('href');
    });
    $(".linea:not(.publicidad):not(.conreserva):not(.fecha):not(.noclick):not(.stamp)").on("click",function() {
        window.location.href = $(this).find('.grid-info a:first').attr('href');
    });

    $(".ocultar").click(function(){
    	$(this).parent().next("div").toggle();
    	if($(this).text() == "Ocultar") {
    		$(this).text("Mostrar");
    	} else {
    		$(this).text("Ocultar");
    	}
	});
	$(".ampliar").click(function(){
    	var img = $("#posterimg").attr("src");
    	img = img.replace("posters","posters/original");
    	$(".ampliarCapa img").attr("src",img);
    	$(".ampliarCapa").show();
	});
	$(".ampliarCapa").click(function(){
		$(".ampliarCapa").hide();
	});

	/*Votar*/
	$("#vote img").on("mouseover",function(){
		var i =  $(this).index() - 1;
		$("#vote img").each(function( index ) {
			if(index <= i) {
				$(this).attr("src","img/interface/starF-24.png");
			} else {
				$(this).attr("src","img/interface/starE-24.png");
			}
		});
	});
	$("#vote img").on("mouseout",function(){
		$("#vote img").each(function( index ) {
			$(this).attr("src","img/interface/starE-24.png");
		});
	});
	$("#vote img").on("click", function() {
    	var points = $(this).attr("data-value");
    	var data = $("#vote #i").text().split("-");
    	var idlugar = data[0];
    	var controller = data[1];
    	if(idlugar != "") {
	    	$.ajax({
	            type: "POST",
	            url: api + "index.php",
	            data: { controller: controller, action: "Update", idlugar: idlugar, op: "vote", points: points }
	        })
	        .done(function( msg ) {
	        	msg = JSON.parse(msg);
            	if(msg.success == true) {
            		$("#vote").attr("id","vote-ok");
            		$("#numvotos").text(parseInt($("#numvotos").text())+1);
	            }
	        });
       }
    });
    /*Fin votar*/

	$(".or-spacer").click(function(){
    	$(this).next("div").toggle();
	});

	$("#cookie_action_close_header").click(function(){
		$("#cookie-law-info-bar").fadeOut();
		setCookie("cookies","1",0);
		return false;
	});

	$("#datepicker").datepicker({
		inline: true,
		onSelect: function(dateText, inst) {
			var url = location.protocol + "//" + document.domain + "/" + location.pathname.split('/')[1];
        	var concat = "/";
		    if(location.pathname.split('/')[1] == "") {
			    concat = "main/";
            }
            window.location.href = url + concat + dateText;
	   },
	   dateFormat: "dd-mm-yy",
	   firstDay: 1,
	   monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
	   dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
	   defaultDate: fecha
	});

	/*Buscador*/
	$("#public_search").on("keyup", function(e) {
    	var code = e.which; // recommended to use e.which, it's normalized across browsers
    	if(code==13)e.preventDefault();

    	var busca = $(this).val();
    	var num = 0;
    });
    var num = $("#celdas li:visible").not(".fecha").not(".publicidad").not(".conreserva").length;
    $("#result_search").text(num);
    /*Fin buscador*/

});

function setCookie(cname,cvalue,exdays) {
	if(exdays != 0) {
		var d = new Date();
		d.setTime(d.getTime()+(exdays*24*60*60*1000));
		var expires = "expires="+d.toGMTString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	} else {
		var d = new Date();
		d.setTime(d.getTime()+(5*365*24*60*60*1000));
		var expires = "expires="+d.toGMTString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	}
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}