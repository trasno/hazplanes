<?php
pageAccessControl(1);

try {
    $act = "";
    if (isset($_POST['a'])) { // Forms
        $action = $_POST['a'];
    } else {
        $action = NULL;
    }
    $cancel = "index.php?p=adminlistteatro";
    if(isset($action) && !empty($action)) {
        $newitem = $_POST;
        $id = formAction($newitem, $action, "idobrateatro");
        $msg['success'] = true;
        $botonesTitulo = "";
    } else {
        $botonesTitulo = '<span id="optTitle"><button type="submit" name="submit" form="newobrateatro">Guardar</button>
                    <button type="button" name="cancel" ng-click="cancel(\''.$cancel.'\')">Cancelar</button></span>';
        if(isset($id) && !empty($id)) {
            $options = array("idobrateatro" => $id);
            $obra = new Obras($options,$db);
            $d = $obra->readAction();
            $d = $d[0];
            $act = "m";
            $d["publicado"] == 0? $d["publicado"] = "" : $d["publicado"] = "checked";
            $d["infantil"] == 0? $d["infantil"] = "" : $d["infantil"] = "checked";
            $d["reserva"] == 0? $d["reserva"] = "" : $d["reserva"] = "checked";
            //Convertimos los horarios de la BD en json para angularjs
            $horarios = horariosToJson($d);
        } else {
            $d = array(
                "idobrateatro" => "",
                "nombre" => "",
                "duracion" => "",
                "genero" => "",
                "sinopsis" => "",
                "notas" => "",
                "web" => "",
                "video" => "",
                "poster" => "",
                "fanart" => "",
                "menciones" => "",
                "publicado" => "checked",
                "infantil" => "",
                "reserva" => "",
                "limitereserva" => ""
                );
            $act = "a";
        }
    }
} catch( Exception $e ) {
    //catch any exceptions and report the problem
    $msg = array();
    $msg['success'] = false;
    $msg['errormsg'] = $e->getMessage();
}
?>
    <script>
        $(document).ready(function() {
            initialize();
        });
    </script>
    <section id='content' ng-app>
        <section id='datos' ng-controller="Controller">
            <?php
            if(isset($msg["errormsg"])) {
                echo "<div class='error'>".$msg["errormsg"]."</div>";
                exit;
            }
            ?>
            <div class="header-form">
                <?php
                if(isset($id)) {
                    echo "<h2>Modificar obra de teatro ".$botonesTitulo."</h2>";
                } else {
                    echo "<h2>Nuevo obra de teatro ".$botonesTitulo."</h2>";
                }
                ?>
            </div>
            <?php if(!isset($msg['success'])) { ?>
            <form name="newobrateatro" id="newobrateatro" class="form" action="index.php?p=adminformobra" method="post" enctype="multipart/form-data">
                <input type="hidden" name="a" value="<?php echo $act; ?>"/>
                <input type="hidden" name="idobrateatro" value="<?php echo $d["idobrateatro"]; ?>"/>
                <div class='divsmall'>
                    <label for="form-nombre">Nombre</label>
                    <input type="text" name="nombre" id="form-nombre" class="campo" required="required" value="<?php echo htmlspecialchars($d["nombre"]); ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-duracion">Duración</label>
                    <input type="number" name="duracion" id="form-duracion" class="campo" value="<?php echo $d["duracion"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-genero">Género</label>
                    <input type="text" name="genero" id="form-genero" class="campo" value="<?php echo $d["genero"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-video">Vídeo</label>
                    <input type="url" name="video" id="form-video" class="campo" value="<?php echo $d["video"]; ?>" />
                </div>
                <div class='divbig'>
                    <label for="form-sinopsis">Sinopsis</label>
                    <textarea name="sinopsis" id="form-sinopsis" class="campo"><?php echo $d["sinopsis"]; ?></textarea>
                </div>
                <div class='divbig'>
                    <label for="form-notas">Notas</label>
                    <textarea name="notas" id="form-notas" class="campo"><?php echo $d["notas"]; ?></textarea>
                </div>
                <div class='divbig'>
                    <label for="form-menciones">Menciones / Agradecimientos / Créditos</label>
                    <textarea name="menciones" id="form-menciones" class="campo"><?php echo $d["menciones"]; ?></textarea>
                </div>
                <div class='divbig'>
                    <label for="form-posterURL">Poster desde URL</label>
                    <input type="url" name="posterURL" id="form-posterURL" class="campo" />
                    <input type="file" style='display:none;' name="poster" id="form-poster" class="campo" />
                    <button type="button" onclick='$("#form-poster").click();'>Subir</button>
                    <?php
                    if(!empty($d["poster"])) {
                        echo "<p style='margin: auto; text-align:center;'><img src='".$d["poster"]."?".strtotime("now")."' alt='Poster de la obra' style='max-width: 300px; max-height: 200px;'/><br><button type='button' name='delImg1' onclick='deleteImage(this,".$d["idobrateatro"].",\"".$d["poster"]."\",\"Obras\")'>Eliminar</button></p>";
                    }
                    ?>
                </div>
                <div class='divbig'>
                    <label for="form-fanartURL">Fanart desde URL</label>
                    <input type="text" name="fanartURL" id="form-fanartURL" class="campo" />
                    <input type="file" style='display:none;' name="fanart" id="form-fanart" class="campo" />
                    <button type="button" onclick='$("#form-fanart").click();'>Subir</button>
                    <?php
                    if(!empty($d["fanart"])) {
                        echo "<p style='margin: auto; text-align:center;'><img src='".$d["fanart"]."?".strtotime("now")."' alt='Fanart de la obra' style='max-width: 300px; max-height: 200px;'/><br><button type='button' name='delImg2' onclick='deleteImage(this,".$d["idobrateatro"].",\"".$d["fanart"]."\",\"Obras\")'>Eliminar</button></p>";
                    }
                    ?>
                </div>
                <div class='divbig'>
                    <div style='display: inline-block;'>
                        <label for="form-publicado">Publicado</label>
                        <span class="checkbox">
                            <input type="checkbox" id="form-publicado" name="publicado" <?php echo $d["publicado"]; ?> />
                            <label class="check" for="form-publicado"></label>
                        </span>
                    </div>
                    <div style='display: inline-block;'>
                        <label for="form-infantil">Infantil</label>
                        <span class="checkbox">
                            <input type="checkbox" id="form-infantil" name="infantil" <?php echo $d["infantil"]; ?> />
                            <label class="check" for="form-infantil"></label>
                        </span>
                    </div>
                    <div style='display: inline-block;'>
                        <label for="form-reserva">Reservar</label>
                        <span class="checkbox">
                            <input type="checkbox" id="form-reserva" name="reserva" <?php echo $d["reserva"]; ?> />
                            <label class="check" for="form-reserva"></label>
                        </span>
                        <input type='datetime' id='form-limitereserva' name='limitereserva' class="campo field-limitereserva" value="<?php echo $d["limitereserva"]; ?>" />
                    </div>
                    <div class='divsmall'>
                        <label for="form-web">Web</label>
                        <input type="text" name="web" id="form-web" class="campo" value="<?php echo $d["web"]; ?>" />
                    </div>
                </div>
                <br>
                <?php
                formHorariosLocales("obrateatro");
                echo "<br>";
                formHorariosAuditorios("obrateatro");
                echo "<br>";
                formHorariosCines("obrateatro");
                echo "<br>";
                formHorariosMuseos("obrateatro");
                echo "<br>";
                formHorariosLugares("obrateatro");
                ?>
            </form>
            <?php } else { ?>
            <div class="form">
                <?php
                if($action == "a") {
                    echo "<p>Obra de teatro creada con éxito.</p>";
                } elseif($action == "m") {
                    echo "<p>Obra de teatro modificada con éxito.</p>";
                }
                echo '<p><a href="index.php?p=adminformobra&i='.$id.'">Modificar</a></p>';
                ?>
                <p>
                    <a href="index.php?p=adminformobra">Crear una nueva</a>
                </p>
                <a href="<?php echo $cancel; ?>"><< Volver al listado</a>
            </div>
            <?php } ?>
        </section>
    </section>
    <script>
        lugares = <?php if(!empty($horarios["horasLugar"])) echo $horarios["horasLugar"]; else echo "[]"; ?>;
        locales = <?php if(!empty($horarios["horasLocal"])) echo $horarios["horasLocal"]; else echo "[]"; ?>;
        cines = <?php if(!empty($horarios["horasCine"])) echo $horarios["horasCine"]; else echo "[]"; ?>;
        auditorios = <?php if(!empty($horarios["horasAuditorio"])) echo $horarios["horasAuditorio"]; else echo "[]"; ?>;
        museos = <?php if(!empty($horarios["horasMuseo"])) echo $horarios["horasMuseo"]; else echo "[]"; ?>;
    </script>