<?php
pageAccessControl(1);

// Validate what page num to show in list
if(isset($_GET["pag"])) {
    $pag = $_GET["pag"];
} else {
    $pag = 0;
}

//Validate what order to apply to list
if(isset($_GET["order"])) {
    $orde = $_GET["order"];
    if(substr($orde,0,2) == "az") {
        $order = substr($orde,2).", fecha desc";
    } else {
        $order = substr($orde,2)." desc, fecha desc";
    }
} else {
    $order = "fecha desc, nombre";
    $orde = "";
}
$start = $pag * 50;
$options = array("limit" => 50, "start" => $start, "order" => $order);
$cursos = new Cursos($options,$db);
$data = $cursos->readAction();
$cont = count($data);
?>
    <section id='content'>
        <section id='datos'>
            <div class="header-list">
                <h2>Cursos <span id="optTitle"><button type="button" onclick="location.href='index.php?p=adminformcurso'">Nuevo curso</button></span></h2>
                <div id="listOptions">
                    <button type="button" c='Cursos' id='publish'>Publicar</button>
                    <button type="button" c='Cursos' id='unpublish'>No publicar</button>
                    <button type="button" c='Cursos' id='delete'>Eliminar</button>
                </div>
            </div>
            <?php paginacion($pag,$cont,"adminlistcursos",$orde); ?>
            <input type="hidden" name="type" id="type" value="cursos"/>
            <table id="list" class="tabla">
                <thead>
                    <th class='listCheckCell'><input type="checkbox" name="checkall" id="" class="" title="Seleccionar/Deseleccionar todo"/></th>
                    <th class='listPublishedCell'><a href="index.php?p=adminlistcursos&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azpublicado"?"zapublicado":"azpublicado"; ?>">Publicado</a></th>
                    <th><a href="index.php?p=adminlistcursos&pag=<?php echo $pag; ?>&order=<?php echo $orde == "aznombre"?"zanombre":"aznombre"; ?>">Nombre</a></th>
                    <th><a href="index.php?p=adminlistcursos&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azlugar"?"zalugar":"azlugar"; ?>">Lugar</a></th>
                    <th><a href="index.php?p=adminlistcursos&pag=<?php echo $pag; ?>&order=<?php echo $orde == "aztipo"?"zatipo":"aztipo"; ?>">Tipo</a></th>
                    <th class='col-fecha-list'><a href="index.php?p=adminlistcursos&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azfecha"?"zafecha":"azfecha"; ?>">Fecha</a></th>
                    <th><a href="index.php?p=adminlistcursos&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azposter"?"zaposter":"azposter"; ?>">Poster</a></th>
                    <th>Enlace</th>
                </thead>
                <tbody id="listrows">
                <?php
                foreach($data as $k => $d) {
                    $link = "curso/";
                    $enlace = BASE_URL.$link.$d["idcurso"]."-".urlAmigable($d["nombre"]);
                    echo "<tr class='row'>
                        <td><input type='checkbox' name='checkListItem' id='".$d["idcurso"]."' class='' title='Seleccionar/Deseleccionar'/></td>
                        <td>".($d["publicado"]?"Si":"No")."</td>
                        <td><a href='index.php?p=adminformcurso&i=".$d["idcurso"]."'>".$d["nombre"]."</a></td>
                        <td>".$d["lugar"]."</td>
                        <td>".$d["tipo"]."</td>
                        <td>".date("d-m-Y H:i",strtotime($d["fecha"]))."</td>
                        <td>".(empty($d["poster"])?"No":"Si")."</td>
                        <td><a href='".$enlace."'>".$enlace."</a></td>
                    </tr>";
                }
                ?>
                </tbody>
            </table>
            <?php paginacion($pag,$cont,"adminlistcursos",$orde); ?>
        </section>
    </section>
