<?php
pageAccessControl(1);
?>
    <section id='content'>
        <div class="grid">
            <section id='datos'>
                <div class="header-list">
                    <h2>Entradas <span id="optTitle"><button type="button" onclick="location.href='index.php?p=adminformentradas'">Nueva venta de entradas</button></span></h2>
                    <div id="listOptions">
                        <button type="button" c='Entradas' id='publish'>Publicar</button>
                        <button type="button" c='Entradas' id='unpublish'>No publicar</button>
                        <button type="button" c='Entradas' id='delete'>Eliminar</button>
                    </div>
                </div>
                <table id="list" class="tabla">
                    <thead>
                        <th class='listCheckCell'><input type="checkbox" name="checkall" id="" class="" title="Seleccionar/Deseleccionar todo"/></th>
                        <th class='listPublishedCell'>Publicado</th>
                        <th>Nombre</th>
                        <th>Poster</th>
                    </thead>
                    <?php
                    $entradas = new Entradas("",$db);
                    $data = $entradas->readAction();
                    foreach($data as $k => $d) {
                        echo "<tr class='row'>
                            <td><input type='checkbox' name='checkListItem' id='".$d["identradas"]."' class='' title='Seleccionar/Deseleccionar'/></td>
                            <td>".($d["publicado"]?"Si":"No")."</td>
                            <td><a href='index.php?p=adminformentradas&i=".$d["identradas"]."'>".$d["nombre"]."</a></td>
                            <td>".(empty($d["imagen"])?"No":"Si")."</td>
                        </tr>";
                    }
                    ?>
                </table>
            </section>
        </div>
    </section>
