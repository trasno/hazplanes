<?php
pageAccessControl(1);
?>
    <section id='content'>
        <div class="grid">
            <section id='datos'>
                <h2><?php echo isset($_POST["date"])?$_POST["date"]:"Sin definir"; ?></h2>
                <form action="#" method="post">
                	<input type="date" name="date" id="date" value="<?php echo isset($_POST["date"])?$_POST["date"]:date("Y-m-d", strtotime("now")); ?>"/>
                	<button type="submit">Enviar</button>
                </form>
                <table id="list" class="tabla" style='margin: 0px;'>
                    <thead>
                        <th>Posts</th>
                    </thead>
                    <?php
                    //Short URL Information
                    /*$shortUrl="http://goo.gl/sNMfXo";
                    $params = array('shortUrl' => $shortUrl, 'key' => $apiKey,'projection' => "ANALYTICS_CLICKS");
                    $info = httpGet($params);
                    if($info != null && isset($info->longUrl)) {
                        echo "Long URL is : ".$info->longUrl."n";
                        echo "All time clicks : ".$info->analytics->allTime->shortUrlClicks."n";

                    }

                    //Get Full Details of the short URL
                    $params = array('shortUrl' => $shortUrl, 'key' => $apiKey,'projection' => "FULL");
                    $info = httpGet($params);
                    var_dump($info);



                    function httpGet($params) {
                        $final_url = 'https://www.googleapis.com/urlshortener/v1/url?'.http_build_query($params);

                        $curlObj = curl_init($final_url);

                        curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
                        curl_setopt($curlObj, CURLOPT_HEADER, 0);
                        curl_setopt($curlObj, CURLOPT_HTTPHEADER, array('Content-type:application/json'));

                        $response = curl_exec($curlObj);

                        //change the response json string to object
                        print_r($response);
                        $json = json_decode($response);
                        curl_close($curlObj);

                        return $json;
                    }*/

                    if(isset($_POST["date"])) {
                    	$date = $_POST["date"];
                        $now = date("Y-m-d 00:00:00", strtotime($date));
                        $fin = date("Y-m-d 23:59:59", strtotime($date));
                        $options = array(
                            "filter" => "(((fecha between '".$now."' and '".$fin."') or (date(fecha) = date('".$now."') and (time(fecha) < '06:00:00' or todoeldia = 1)))) and publicado = 1",
                            "sharelinks" => 1
                            //"agenda" => 1
                            );
                        $general = new GeneralController($options,$db);
                        $data = $general->readAction();
                        $temp = array();
                        foreach($data as $k => $d) {
                            if(date("G",strtotime($d["fecha"])) < 6 && (date("H:i:s",strtotime($d["fecha"])) != "00:00:00" || $d["tipoevento"] == "exposiciones" || $d["tipoevento"] == "peliculas")) {
                                array_push($temp, $d);
                                unset($data[$k]);
                            }
                        }
                        $data = array_merge($data, $temp);
                        $ids = array();
                        foreach($data as $k => $d) {
                            if(in_array($d["nombre"],$ids)) {
                                continue;
                            }
                            $ids[] = $d["nombre"];
                            switch($d["tipoevento"]) {
                                case "peliculas":
                                    $type = "Película";
                                    $link = "pelicula/";
                                    $adminlink = "adminformfilm";
                                    $idString = "idpelicula";
                                break;
                                case "conciertos":
                                    $type = "Concierto";
                                    $link = "concierto/";
                                    $adminlink = "adminformconcert";
                                    $idString = "idconcierto";
                                break;
                                case "obrasteatro":
                                    $type = "Teatro";
                                    $link = "obra/";
                                    $adminlink = "adminformobra";
                                    $idString = "idobrateatro";
                                break;
                                case "eventos":
                                    $type = "Evento";
                                    $link = "evento/";
                                    $adminlink = "adminformevent";
                                    $idString = "ideventos";
                                break;
                                case "exposiciones":
                                    $type = "Exposición";
                                    $link = "exposicion/";
                                    $adminlink = "adminformexpo";
                                    $idString = "idexposicion";
                                break;
                                case "deportes":
                                    $type = "Deporte";
                                    $link = "competicion/";
                                    $adminlink = "adminformdeporte";
                                    $idString = "iddeporte";
                                break;
                                case "formacion":
                                    $type = "Cursos";
                                    $link = "curso/";
                                    $adminlink = "adminformcurso";
                                    $idString = "idcurso";
                                break;
                            }
                            //Long to Short URL
                            $shortUrl = "";
                            if(empty($d["shortUrl"])) {
                                $enlace = BASE_URL_SSL.$link.$d["id"]."-".urlAmigable($d["nombre"]);
                                $postData = array('longUrl' => $enlace);
                                $info = getShortURL($postData);
                                if(isset($info->id) && !empty($info->id)) {
                                    $shortUrl = $info->id;
                                    $newitem = array($idString => $d["id"], "shortUrl" => $shortUrl, "op" => "field");
                                    formAction($newitem, 'm', $idString);
                                } else {
                                    print_r($info->error);
                                }
                                flush();
                                ob_flush();
                                sleep(1);
                            } else {
                                $shortUrl = $d["shortUrl"];
                            }
                            if(!empty($d["genero"])) {
                                $d["genero"] = " | ".$d["genero"];
                            }
                            if(!empty($d["localidad"])) {
                                $d["localidad"] = " (".$d["localidad"].")";
                            }
                            if(date("H:i",strtotime($d["fecha"])) == "00:00") {
                                $fecha = date("d/m/y",strtotime($d["fecha"]));
                            } else {
                                $fecha = date("d/m/y H:i",strtotime($d["fecha"]));
                            }
                            $text = substr($fecha." - ".$type.$d["genero"].$d["localidad"]." - ".$d["nombre"],0,119);
                            echo "<tr class='row'>
                                <td>".$text." ".$shortUrl."</td>
                            </tr>";
                        }
                    }
                    ?>
                </table>
            </section>
        </div>
    </section>
