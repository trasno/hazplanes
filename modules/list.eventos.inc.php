<?php
pageAccessControl();
?>
    <section id='content' class='<?php echo $displayContent; ?>'>
        <div class="grid">
            <section id="main">
                <ul id="celdas">
                    <?php
                    if($filtroFecha == "+0" || $filtroFecha == date("d-m-Y", strtotime("now"))) {
                        $now = date("Y-m-d H:i:s", strtotime("now"));
                        $fin = date("Y-m-d 05:59:59", strtotime("today"));
                        $now2 = date("Y-m-d H:i:s", strtotime("now -1 day"));
                        $fin2 = date("Y-m-d 05:59:59", strtotime("today -1 day"));
                        $options = array(
                            "filter" => "(fecha >= '".$now."' or (date(fecha) = date('".$now."') and (time(fecha) < '06:00:00' or todoeldia = 1)) or fecha between '".$now2."' and '".$fin2."') and publicado = 1",
                            "agenda" => 1
                            );
                    } else {
                        $now = date("Y-m-d 00:00:00", strtotime($filtroFecha));
                        $fin = date("Y-m-d 23:59:59", strtotime($filtroFecha));
                        $options = array(
                            "filter" => "(fecha between '".$now."' and '".$fin."') and publicado = 1",
                            "agenda" => 1
                            );
                    }
                    switch($p) {
                        case "cartelera":
                            $eventos = new Peliculas($options,$db);
                            $page = "pelicula";
                            $celdaClass = "film";
                            $horaDespues = "Primera sesión";
                            $horaHoy = "Próx. sesión";
                            $idevent = "idpelicula";
                            $imgnull = "null-film.png";
                            break;
                        case "conciertos":
                            $eventos = new Conciertos($options,$db);
                            $celdaClass = "concert";
                            $page = "concierto";
                            $horaDespues = "Hora";
                            $horaHoy = "Hora";
                            $idevent = "idconcierto";
                            $imgnull = "null-music.png";
                            break;
                        case "deportes":
                            $eventos = new Deportes($options,$db);
                            $celdaClass = "sport";
                            $page = "competicion";
                            $horaDespues = "Hora";
                            $horaHoy = "Hora";
                            $idevent = "iddeporte";
                            $imgnull = "null-sports.png";
                            break;
                        case "obrasteatro":
                            $eventos = new Obras($options,$db);
                            $celdaClass = "theater";
                            $page = "obra";
                            $horaDespues = "Hora";
                            $horaHoy = "Hora";
                            $idevent = "idobrateatro";
                            $imgnull = "null-theatre.png";
                            break;
                        case "eventos":
                            $eventos = new Eventos($options,$db);
                            $celdaClass = "event";
                            $page = "evento";
                            $horaDespues = "Hora";
                            $horaHoy = "Hora";
                            $idevent = "ideventos";
                            $imgnull = "null-calendar.png";
                            break;
                        case "formacion":
                        case "cursos":
                            $eventos = new Cursos($options,$db);
                            $celdaClass = "course";
                            $page = "curso";
                            $horaDespues = "Hora";
                            $horaHoy = "Hora";
                            $idevent = "idcurso";
                            $imgnull = "null-cursos.png";
                            break;
                        case "exposiciones":
                            if($filtroFecha == "+0"/* || $filtroFecha == date("d-m-Y", strtotime("now"))*/) {
                                $options["filter"] = "(fin >= CURDATE() or permanente = 1) and publicado = 1";
                                $filtroFecha = "now";
                            } else {
                                $options["filter"] = "((inicio <= '".date("Y-m-d",strtotime($filtroFecha))."' and fin >= '".date("Y-m-d",strtotime($filtroFecha))."') or permanente = 1) and publicado = 1";
                            }
                            $eventos = new Exposiciones($options,$db);
                            $celdaClass = "expo";
                            $page = "exposicion";
                            $temporales = "";
                            $permanentes = "";
                            $futuras = "";
                            $idevent = "idexposicion";
                            $imgnull = "null-museum.png";
                            break;
                    }
                    if(isset($eventos) && !empty($eventos)) {
                        $data = $eventos->readAction();
                    } else {
                        $data = array();
                    }
                    $publi = 0;
                    $countCells = 0;
                    if(count($data) < 1) {
                        if($filtroFecha == "+0") {
                            echo "<li class='".$display." noclick'>
                                <div class='grid-info no-events'>
                                    <br><br>No hay eventos en esta categoría.
                                </div>
                            </li>";
                        } else {
                            echo "<li class='".$display." noclick'>
                                <div class='grid-info no-events'>
                                    <br><br>No hay eventos para este día.
                                </div>
                            </li>";
                        }
                    } else {
                        $fecha = "";
                        $hoy = date("d-m-Y", strtotime("now"));
                        $mañana = date("d-m-Y", strtotime("+1 day"));
                        $ids = array();
                        if($p != "exposiciones") {
                            $temp = array();
                            $dia = "";
                            $temp2 = $temp3 = array();
                            foreach($data as $k => $d) {
                                if($dia == date("d-m-Y", strtotime($d["fecha"]))) {
                                    if(date("G",strtotime($d["fecha"])) < 6 && (date("H:i:s",strtotime($d["fecha"])) != "00:00:00" || $p == "exposiciones" || $p == "peliculas")) {
                                        array_push($temp, $d);
                                    } else {
                                        array_push($temp3, $d);
                                    }
                                } else {
                                    $dia = date("d-m-Y", strtotime($d["fecha"]));
                                    $temp2 = array_merge($temp2, $temp3, $temp);
                                    $temp = $temp3 = array();
                                    if(date("G",strtotime($d["fecha"])) < 6 && (date("H:i:s",strtotime($d["fecha"])) != "00:00:00" || $p == "exposiciones" || $p == "peliculas")) {
                                        array_push($temp, $d);
                                    } else {
                                        array_push($temp3, $d);
                                    }
                                }
                            }
                            $temp2 = array_merge($temp2, $temp3, $temp);
                            $data = $temp2;
                        }
                        foreach($data as $k => $d) {
                            if($p == "exposiciones") {
                                $d["fecha"] = $d["inicio"];
                            } else if($p == "formacion" || $p == "eventos") {
                                $d["genero"] = $d["tipo"];
                            }
                            $bdfecha = date("d-m-Y",strtotime($d["fecha"]));
                            if(in_array($d["nombre"].$bdfecha,$ids)) {
                                continue;
                            }
                            $countCells++;
                            if($countCells == 3) {
                                echo $celdaPubli;
                                $publi++;
                            }
                            if($p != "exposiciones") {
                                if($fecha != $bdfecha) {
                                    if($fecha != "") {
                                        if($publi < 2) {
                                            echo $celdaPubli;
                                            $publi++;
                                        }
                                    }
                                    if($bdfecha == $hoy) {
                                        $f = "Hoy";
                                    } elseif($bdfecha == $mañana) {
                                        $f = "Mañana";
                                    } else {
                                        $f = $bdfecha;
                                    }
                                    $f = $dias[date("N",strtotime($bdfecha))-1]." ".date("d",strtotime($bdfecha))." de ".$meses[date("n",strtotime($bdfecha))-1]." de ".date("Y",strtotime($bdfecha))." (".$f.")";
                                    echo "<li class='".$display." fecha'>".$f."</li>";
                                }
                            } else {
                                if(!$d["permanente"]) {
                                    if($d["inicio"] > date("Y-m-d", strtotime($filtroFecha)) && empty($futuras)) {
                                        echo "<li class='".$display." fecha'>Exposiciones futuras</li>";
                                        $futuras = "Ok";
                                    } elseif(empty($temporales)) {
                                        echo "<li class='".$display." fecha'>Exposiciones temporales</li>";
                                        $temporales = "Ok";
                                    }
                                } elseif(empty($permanentes)) {
                                    echo "<li class='".$display." fecha'>Exposiciones permanentes</li>";
                                    $permanentes = "Ok";
                                }
                            }
                            $fecha = $bdfecha;
                            $ids[] = $d["nombre"].$bdfecha;
                            $img = "";
                            if($p == "deportes") {
                                $img = "img/deportes/tipos/mix.png";
                                if(!empty($d["genero"])) {
                                    $options2 = array("read" => "datatipodeporte", "idtipodeporte" => $d["tipo"]);
                                    $tipodeporte = new Deportes($options2,$db);
                                    $d2 = $tipodeporte->readAction();
                                    if(!empty($d2)) {
                                        $d2 = $d2[0];
                                        if(!empty($d2["poster"])) {
                                            $img = "img/deportes/tipos/".$d2["poster"];
                                        }
                                        $d["genero"] = $d2["nombre"];
                                    } else {
                                        $d["genero"] = "Sin especificar";
                                    }
                                } else {
                                    $d["genero"] = "Sin especificar";
                                }
                            }
                            if(!empty($d["poster"])) {
                                if(strpos($d["poster"],'/') !== false) {
                                    $img = explode("/",$d["poster"]);
                                    $img[4] = $img[3];
                                    $img[3] = "thumbs";
                                    $img = implode("/",$img);
                                }
                            }
                            if(empty($img) || !file_exists($img)) {
                                $img = "img/interface/".$imgnull;
                            }
                            echo "<li class='".$display." ".$celdaClass."' style='background: url(".$img.") no-repeat center'>";
                            $infostatus = "";
                            if($d["cancelado"]) {
                                echo "<div class='ribbon-wrapper'><div class='ribbon-cancelado'>CANCELADO</div></div>";
                                $infostatus = "cancelado";
                            } else {
                               if($d["agotado"]) {
                                   echo "<div class='ribbon-wrapper'><div class='ribbon-agotado'>AGOTADO</div></div>";
                                   $infostatus = "agotado";
                               } else {
                                   if($d["infantil"]) {
                                       echo "<div class='ribbon-wrapper'><div class='ribbon-infantil'>NIÑOS</div></div>";
                                       $infostatus = "infantil";
                                       if(isset($d["reserva"]) && $d["reserva"]) {
                                           echo "<div class='ribbon-wrapper-2'><div class='ribbon-reserva'>RESERVAR</div></div>";
                                       }
                                       if(isset($d["fechaestreno"]) && ($d["fechaestreno"] == 1 || $d["fechaestreno"] == date("Y-m-d",strtotime($d["fecha"])))) {
                                           echo "<div class='ribbon-wrapper-2'><div class='ribbon-estreno'>ESTRENO</div></div>";
                                       }
                                   } else {
                                       if(isset($d["reserva"]) && $d["reserva"]) {
                                           echo "<div class='ribbon-wrapper'><div class='ribbon-reserva'>RESERVAR</div></div>";
                                           $infostatus = "reserva";
                                       }
                                       if(isset($d["fechaestreno"]) && ($d["fechaestreno"] == 1 || $d["fechaestreno"] == date("Y-m-d",strtotime($d["fecha"])))) {
                                           echo "<div class='ribbon-wrapper'><div class='ribbon-estreno'>ESTRENO</div></div>";
                                       }
                                   }
                               }
                            }
                            echo "        <div class='grid-info ".$infostatus."'>
                                        <h2><a href='".$page."/".$d[$idevent]."-".urlAmigable($d["nombre"])."'>".$d["nombre"]."</a></h2>
                                        <br>
                                        <span class='genre'>".$d["genero"]."</span>
                                        <br>";
                            if($p != "exposiciones") {
                                if($bdfecha != $hoy) {
                                    if(date("H:i",strtotime($d["fecha"])) == "00:00" && $p != "cartelera") {
                                        echo  "      <span class='hour'>Hora sin determinar</span>";
                                    } else {
                                        echo "      <span class='hour'>".$horaDespues.": ".date("H:i",strtotime($d["fecha"]))."</span>";
                                    }
                                } else {
                                    $datetime2 = date_create($d["fecha"]);
                                    if(date("G",strtotime($d["fecha"])) < 6 && date("d",strtotime($d["fecha"])) == date("d",strtotime("now"))) {
                                        $datetime1 = date_create("now -1 day");
                                    } else {
                                        $datetime1 = date_create("now");
                                    }
                                    $interval = date_diff($datetime1, $datetime2);
                                    if($interval->format("%r%I") < 0) {
                                        $intervalText = "Empezó hace";
                                    } else {
                                        $intervalText = "En";
                                    }
                                    if(date("H:i",strtotime($d["fecha"])) == "00:00" && $p != "cartelera") {
                                        echo  "      <span class='hour'>Hora sin determinar</span>";
                                    } else {
                                        $faltaFormat = '%Im';
                                        if($interval->format('%h') > 0) {
                                            $faltaFormat = '%hh %Im';
                                        }
                                        if($interval->format('%d') > 0) {
                                            $faltaFormat = '%dd %hh %Im';
                                        }
                                        echo "      <span class='hour'>".$horaHoy.": ".date("H:i",strtotime($d["fecha"]))." (".$intervalText.": ".$interval->format($faltaFormat).")";
                                        if($d["todoeldia"]) {
                                            echo " (Todo el día)";
                                        }
                                        echo "</span>";
                                    }
                                }
                            } else {
                                if(!$d["permanente"]) {
                                    echo "      <span class='hour'>Finaliza el ".$dias[date("N",strtotime($d["fin"]))-1]." ".date("d",strtotime($d["fin"]))." de ".$meses[date("n",strtotime($d["fin"]))-1]." de ".date("Y",strtotime($d["fin"]))."</span>";
                                } elseif($d["inicio"] > date("Y-m-d", strtotime("now"))) {
                                    echo "      <span class='hour'>
                                            Empieza el ".$dias[date("N",strtotime($d["inicio"]))-1]." ".date("d",strtotime($d["inicio"]))." de ".$meses[date("n",strtotime($d["inicio"]))-1]." de ".date("Y",strtotime($d["inicio"]))."
                                            <br>Finaliza el ".$dias[date("N",strtotime($d["fin"]))-1]." ".date("d",strtotime($d["fin"]))." de ".$meses[date("n",strtotime($d["fin"]))-1]." de ".date("Y",strtotime($d["fin"]))."</span>";
                                }
                                $d["cerrado"] = explode(",",$d["cerrado"]);
                                if(in_array(date("N",strtotime($filtroFecha)), $d["cerrado"])) {
                                    if(date("Y-m-d",strtotime($filtroFecha)) == date("Y-m-d",strtotime("now"))) {
                                        echo "<div class='museo-closed'>Museo cerrado hoy</div>";
                                    } else {
                                        echo "<div class='museo-closed'>Museo cerrado en esta fecha</div>";
                                    }
                                }
                            }
                            echo "  </div>";
                            echo "</li>";
                        }
                    }
                    if($publi < 2) {
                        echo $celdaPubli;
                        if($countCells < 2) {
                            echo $celdaPubli;
                        }
                    }
                    ?>
                </ul>
            </section>
        </div>
    </section>
