<?php
pageAccessControl();
?>
    <section id='content' class='<?php echo $displayContent; ?>'>
        <div class="grid">
            <section id="main">
                <ul id="celdas">
                    <?php
                    if($filtroFecha == "+0" || $filtroFecha == date("d-m-Y", strtotime("now"))) {
                        $now = date("Y-m-d H:i:s", strtotime("now"));
                        $fin = date("Y-m-d 05:59:59", strtotime("today"));
                        $now2 = date("Y-m-d H:i:s", strtotime("now -1 day"));
                        $fin2 = date("Y-m-d 05:59:59", strtotime("today -1 day"));
                        $options = array(
                            "filter" => "(fecha >= '".$now."' or (date(fecha) = date('".$now."') and (time(fecha) < '06:00:00' or todoeldia = 1)) or fecha between '".$now2."' and '".$fin2."') and publicado = 1 and infantil = 1",
                            "agenda" => 1
                            );
                    } else {
                        $now = date("Y-m-d 00:00:00", strtotime($filtroFecha));
                        $fin = date("Y-m-d 23:59:59", strtotime($filtroFecha));
                        $options = array(
                            "filter" => "(fecha between '".$now."' and '".$fin."') and publicado = 1 and infantil = 1",
                            "agenda" => 1
                            );
                    }
                    $general = new GeneralController($options,$db);
                    $data = $general->readAction();
                    $publi = 0;
                    $countCells = 0;
                    if(count($data) < 1) {
                        if($filtroFecha == "+0") {
                            echo "<li class='".$display." noclick'>
                                <div class='grid-info no-events'>
                                    <br><br>No hay eventos en esta categoría.
                                </div>
                            </li>";
                        } else {
                            echo "<li class='".$display." noclick'>
                                <div class='grid-info no-events'>
                                    <br><br>No hay eventos para este día.
                                </div>
                            </li>";
                        }
                    } else {
                        $fecha = "";
                        $hoy = date("d-m-Y", strtotime("now"));
                        $mañana = date("d-m-Y", strtotime("+1 day"));
                        $ids = array();
                        $temp = array();
                        $dia = "";
                        $temp2 = $temp3 = array();
                        foreach($data as $k => $d) {
                            if($dia == date("d-m-Y", strtotime($d["fecha"]))) {
                                if(date("G",strtotime($d["fecha"])) < 6 && (date("H:i:s",strtotime($d["fecha"])) != "00:00:00" || $d["tipoevento"] == "exposiciones" || $d["tipoevento"] == "peliculas")) {
                                    array_push($temp, $d);
                                } else {
                                    array_push($temp3, $d);
                                }
                            } else {
                                $dia = date("d-m-Y", strtotime($d["fecha"]));
                                $temp2 = array_merge($temp2, $temp3, $temp);
                                $temp = $temp3 = array();
                                if(date("G",strtotime($d["fecha"])) < 6 && (date("H:i:s",strtotime($d["fecha"])) != "00:00:00" || $d["tipoevento"] == "exposiciones" || $d["tipoevento"] == "peliculas")) {
                                    array_push($temp, $d);
                                } else {
                                    array_push($temp3, $d);
                                }
                            }
                        }
                        $temp2 = array_merge($temp2, $temp3, $temp);
                        $data = $temp2;
                        foreach($data as $k => $d) {
                            $bdfecha = date("d-m-Y",strtotime($d["fecha"]));
                            if(in_array($d["nombre"].$bdfecha,$ids)) {
                                continue;
                            }
                            $countCells++;
                            if($countCells == 3) {
                                echo $celdaPubli;
                                $publi++;
                            }
                            if($fecha != $bdfecha) {
                                if($fecha != "") {
                                    if($publi < 2) {
                                        echo $celdaPubli;
                                        $publi++;
                                    }
                                }
                                if($bdfecha == $hoy) {
                                    $f = "Hoy";
                                } elseif($bdfecha == $mañana) {
                                    $f = "Mañana";
                                } else {
                                    $f = $bdfecha;
                                }
                                $f = $dias[date("N",strtotime($bdfecha))-1]." ".date("d",strtotime($bdfecha))." de ".$meses[date("n",strtotime($bdfecha))-1]." de ".date("Y",strtotime($bdfecha))." (".$f.")";
                                echo "<li class='".$display." fecha'>".$f."</li>";
                            }
                            $fecha = $bdfecha;
                            $ids[] = $d["nombre"].$bdfecha;
                            $img = "";
                            $type = $d["tipoevento"];
                            if($type == "deportes") {
                                $img = "img/deportes/tipos/mix.png";
                                if(!empty($d["genero"])) {
                                    $options2 = array("read" => "datatipodeporte", "idtipodeporte" => $d["genero"]);
                                    $tipodeporte = new Deportes($options2,$db);
                                    $d2 = $tipodeporte->readAction();
                                    if(!empty($d2)) {
                                        $d2 = $d2[0];
                                        if(!empty($d2["poster"])) {
                                            $img = "img/deportes/tipos/".$d2["poster"];
                                        }
                                        $d["genero"] = $d2["nombre"];
                                    } else {
                                        $d["genero"] = "Sin especificar";
                                    }
                                } else {
                                    $d["genero"] = "Sin especificar";
                                }
                            }
                            if(!empty($d["poster"])) {
                                if(strpos($d["poster"],'/') !== false) {
                                    $img = explode("/",$d["poster"]);
                                    $img[4] = $img[3];
                                    $img[3] = "thumbs";
                                    $img = implode("/",$img);
                                }
                            }
                            $datetime1 = date_create($d["fecha"]);
                            if(date("G",strtotime($d["fecha"])) < 6 && date("d",strtotime($d["fecha"])) == date("d",strtotime("now"))) {
                                $datetime2 = date_create("now -1 day");
                            } else {
                                $datetime2 = date_create("now");
                            }
                            $interval = date_diff($datetime1, $datetime2);
                            $faltaFormat = '%Im';
                            if($interval->format('%h') > 0) {
                                $faltaFormat = '%hh %Im';
                            }
                            if($interval->format('%d') > 0) {
                                $faltaFormat = '%dd %hh %Im';
                            }
                            $imgnull = "";
                            switch($type) {
                                case "peliculas":
                                    $class = "film";
                                    $imgnull = "null-film.png";
                                    $type = "PELICULA";
                                    $link = "pelicula/";
                                break;
                                case "conciertos":
                                    $class = "concert";
                                    $imgnull = "null-music.png";
                                    $type = "CONCIERTO";
                                    $link = "concierto/";
                                break;
                                case "obrasteatro":
                                    $class = "theater";
                                    $imgnull = "null-theatre.png";
                                    $type = "TEATRO";
                                    $link = "obra/";
                                break;
                                case "eventos":
                                    $class = "event";
                                    $imgnull = "null-calendar.png";
                                    $type = "EVENTO";
                                    $link = "evento/";
                                break;
                                case "exposiciones":
                                    $class = "expo";
                                    $imgnull = "null-museum.png";
                                    $type = "EXPOSICIÓN";
                                    $d["hora"] = "En horario del museo";
                                    $link = "exposicion/";
                                break;
                                case "deportes":
                                    $class = "sport";
                                    $imgnull = "null-sports.png";
                                    $type = "DEPORTE";
                                    $link = "competicion/";
                                break;
                                case "formacion":
                                case "cursos":
                                    $class = "course";
                                    $imgnull = "null-cursos.png";
                                    $type = "FORMACIÓN";
                                    $link = "curso/";
                                break;
                            }
                            if($type == "peliculas") {
                                if($filtroFecha == "+0" || $filtroFecha == date("d-m-Y", strtotime("now"))) {
                                    $d["hora"] = "Próx. sesión: ".date("H:i",strtotime($d["fecha"]))." (En: ".$interval->format($faltaFormat).")";
                                } else {
                                    $d["hora"] = "Primera sesión: ".date("H:i",strtotime($d["fecha"]));
                                }
                            } elseif($type != "exposiciones") {
                                if(strtotime($d["fecha"]) > strtotime("now")) {
                                    $d["hora"] = "Hora: ".date("H:i",strtotime($d["fecha"]))." (En: ".$interval->format($faltaFormat).")";
                                } else {
                                    $d["hora"] = "Hora: ".date("H:i",strtotime($d["fecha"]))." (Lleva: ".$interval->format($faltaFormat).")";
                                }
                            }
                            if(empty($img) || !file_exists($img)) {
                                $img = "img/interface/".$imgnull;
                            }
                            echo "<li class='".$display." ".$class."' data-category='".$class."' style='background: url(".$img.") no-repeat center'>";
                            $infostatus = "";
                            if($d["cancelado"]) {
                                echo "<div class='ribbon-wrapper'><div class='ribbon-cancelado'>CANCELADO</div></div>";
                                $infostatus = "cancelado";
                            } else {
                               if($d["agotado"]) {
                                   echo "<div class='ribbon-wrapper'><div class='ribbon-agotado'>AGOTADO</div></div>";
                                   $infostatus = "agotado";
                               } else {
                                   if($d["reserva"]) {
                                       echo "<div class='ribbon-wrapper'><div class='ribbon-reserva'>RESERVAR</div></div>";
                                       $infostatus = "reserva";
                                   }
                               }
                            }
                            echo "        <div class='grid-info ".$infostatus."'>
                                        <h2><a href='".$link.$d["id"]."-".urlAmigable($d["nombre"])."'>".$d["nombre"]."</a></h2>
                                        <br>
                                        <span class='genre'>".$d["genero"]."</span>
                                        <br>
                                        <span class='hour'>".$d["hora"]."</span>
                                        <span class='type'>".$type."</span>
                                    </div>
                                </li>";
                        }
                        if($publi < 2) {
                            echo $celdaPubli;
                            if($countCells < 2) {
                                echo $celdaPubli;
                            }
                        }
                    }
                    ?>
                </ul>
            </section>
        </div>
    </section>
