<?php
pageAccessControl();
?>
    <section id='content'>
        <div class="grid">
            <section id="main">
                <div id='loginbox'>
                    <div id='box_data' class='title'>
                        <?php echo PAGENAME; ?><br/><span style='font-size: 9px;'><?php echo VERSION; ?></span>
                    </div>
                    <?php
                        if(!empty($error)) {
                            echo "<div id='errorbox'>
                                    <div id='errorbox_data' class='content'>
                                        ".$error."
                                    </div>
                                </div>";
                        }
                    ?>
                    <form name='loginform' id='loginform' method='post' class='login' action='index.php?p=loginadmin'>
                        <p>
                            <input type="email" name="loginbox_email" id="login" class='campo' placeholder="Email" autofocus>
                        </p>
                        <p>
                            <input type="password" name="loginbox_password" id="password" class='campo' placeholder="Contraseña">
                        </p>
                        <p>
                            <label for="remember">¿Recordar?</label>
                            <span class="checkbox">
                                <input type="checkbox" id="form-remember" name="loginbox_remember" />
                                <label class="check" for="form-remember"></label>
                            </span>
                        </p>
                        <p class="login-submit">
                            <button type="submit" class="login-button boton">Entrar</button>
                        </p>
                    </form>
                </div>
            </section>
        </div>
    </section>
