<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
</head>
<body>
<?php
/*
 * Scraper de reservaentradas (version nueva de la web)
 * Modificado el 03-10-2014
 */

$opts = array('http'=>array('header' => "User-Agent:MyAgent/1.0\r\n"));
$context = stream_context_create($opts);

date_default_timezone_set("Europe/Madrid");

echo "<pre>";
$sleep = "2";
$mesesInv = array("enero" => "01",
        "febrero" => "02",
        "marzo" => "03",
        "abril" => "04",
        "mayo" => "05",
        "junio" => "06",
        "julio" => "07",
        "agosto" => "08",
        "septiembre" => "09",
        "octubre" => "10",
        "noviembre" => "11",
        "diciembre" => "12");

//Plaza Eliptica
echo "<h1>Plaza eliptica</h1>";
$url = "http://cinesplazaeliptica.reservaentradas.com/cgi-vel/CINES/ww-index.pro";
$output = file_get_contents($url,false,$context);

preg_match_all('/w\-peli=(.*?)\"/s', $output, $idpelis);

$idpelis = array_unique($idpelis[1]);
$count = 0;
$tot = count($idpelis);
foreach($idpelis as $idpeli) {
    $count++;
    $url = "http://cinesplazaeliptica.reservaentradas.com/cgi-vel/CINES/ww-sesiones.pro?w-peli=".$idpeli;
    $texto = file_get_contents($url,false,$context);

    preg_match_all('/<legend (.*?)>(.*?)<\/legend>/s', $texto, $dias);
    if(isset($dias[0][1])) {
        preg_match_all('/class=\"span7\"(.*?)<\/div>/s', $texto, $horas);
        preg_match_all('/entradas <b>(.*?)<\/b>/s', $dias[0][1], $titulo);

        if(substr($titulo[1][0],0,4) == "(3D)") {
            $ddd = 1;
        } else {
            $ddd = 0;
        }
        $titulo = str_replace("(HD) ","",str_replace("(3D) ","",$titulo[1][0]));
        $titulo = ucwords(str_replace("Á","á",str_replace("É","é",str_replace("Í","í",str_replace("Ó","ó",str_replace("Ú","ú",str_replace("Ñ","ñ",strtolower(utf8_encode($titulo)))))))));
        if(strpos($titulo,'vose') !== false || strpos($titulo,'v.o.s.e.') !== false) {
            $vo = 1;
            $titulo = trim(str_replace("(v.o.s.e.)","",str_replace("(vose)","",trim($titulo))));
        } else {
            $vo = 0;
        }
        $dias[2] = array_slice($dias[2],2);
        $i = 0;
        foreach($dias[2] as $dia) {
            $fecha = explode(" ",strip_tags($dia));
            $fech = $fecha[5]."-".$mesesInv[strtolower($fecha[3])]."-".$fecha[1];

            preg_match_all('/href=\"(.*?)ses=(.*?)\"(.*?)<b>SALA (.*?)<\/b>(.*?)sesion\">(.*?)<\/span>/s',$horas[1][$i],$hora);
            for($a = 0; $a < count($hora[4]); $a++) {
                $cines[$titulo]["Plaza Elíptica"][strtotime($fech)][] = array("sala" => $hora[4][$a], "hora" => $hora[6][$a], "url" => "http://cinesplazaeliptica.reservaentradas.com/cgi-vel/CINES/ww-sel-loc.pro?w-ses=".$hora[2][$a], "3D" => $ddd, "vo" => $vo);
            }
            $i++;
        }
    }
    echo $count." de ".$tot." - ".$titulo."<br>";
    @ob_end_flush();
    @ob_flush();
    @flush();
    @ob_start();
    sleep($sleep);
}

//Multicines Norte
echo "<h1>Multicines Norte</h1>";
$url = "http://multicinesnorte.reservaentradas.com/cgi-vel/CINES/ww-index.pro";
$output = file_get_contents($url,false,$context);

preg_match_all('/w\-peli=(.*?)\"/s', $output, $idpelis);

$idpelis = array_unique($idpelis[1]);
$count = 0;
$tot = count($idpelis);
foreach($idpelis as $idpeli) {
    $count++;
    $url = "http://multicinesnorte.reservaentradas.com/cgi-vel/CINES/ww-sesiones.pro?w-peli=".$idpeli;
    $texto = file_get_contents($url,false,$context);

    preg_match_all('/<legend (.*?)>(.*?)<\/legend>/s', $texto, $dias);
    if(isset($dias[0][1])) {
        preg_match_all('/class=\"span7\"(.*?)<\/div>/s', $texto, $horas);
        preg_match_all('/entradas <b>(.*?)<\/b>/s', $dias[0][1], $titulo);

        if(substr($titulo[1][0],0,4) == "(3D)") {
            $ddd = 1;
        } else {
            $ddd = 0;
        }
        $titulo = trim(str_replace("(HD) ","",str_replace("(3D) ","",trim($titulo[1][0]))));
        $titulo = ucwords(str_replace("Á","á",str_replace("É","é",str_replace("Í","í",str_replace("Ó","ó",str_replace("Ú","ú",str_replace("Ñ","ñ",strtolower(utf8_encode($titulo)))))))));
        if(strpos($titulo,'vose') !== false || strpos($titulo,'v.o.s.e.') !== false) {
            $vo = 1;
            $titulo = trim(str_replace("(v.o.s.e.)","",str_replace("(vose)","",trim($titulo))));
        } else {
            $vo = 0;
        }
        $dias[2] = array_slice($dias[2],2);
        $i = 0;
        foreach($dias[2] as $dia) {
            $fecha = explode(" ",strip_tags($dia));
            $fech = $fecha[5]."-".$mesesInv[strtolower($fecha[3])]."-".$fecha[1];

            preg_match_all('/href=\"(.*?)ses=(.*?)\"(.*?)<b>SALA (.*?)<\/b>(.*?)sesion\">(.*?)<\/span>/s',$horas[1][$i],$hora);
            for($a = 0; $a < count($hora[6]); $a++) {
                $cines[$titulo]["Multicines Norte"][strtotime($fech)][] = array("sala" => $hora[4][$a], "hora" => $hora[6][$a], "url" => "http://multicinesnorte.reservaentradas.com/cgi-vel/CINES/ww-sel-loc.pro?w-ses=".$hora[2][$a], "3D" => $ddd, "vo" => $vo);
            }
            $i++;
        }
    }
    echo $count." de ".$tot." - ".$titulo."<br>";
    @ob_end_flush();
    @ob_flush();
    @flush();
    @ob_start();
    sleep($sleep);
}

//Gran vía cines
echo "<h1>Gran vía</h1>";
$url = "http://granviacines.reservaentradas.com/cgi-vel/CINES/ww-index.pro";
$output = file_get_contents($url,false,$context);

preg_match_all('/w\-peli=(.*?)\"/s', $output, $idpelis);

$idpelis = array_unique($idpelis[1]);
$count = 0;
$tot = count($idpelis);
foreach($idpelis as $idpeli) {
    $count++;
    $url = "http://granviacines.reservaentradas.com/cgi-vel/CINES/ww-sesiones.pro?w-peli=".$idpeli;
    $texto = file_get_contents($url,false,$context);

    preg_match_all('/<legend (.*?)>(.*?)<\/legend>/s', $texto, $dias);
    if(isset($dias[0][1])) {
        preg_match_all('/class=\"span7\"(.*?)<\/div>/s', $texto, $horas);
        preg_match_all('/entradas <b>(.*?)<\/b>/s', $dias[0][1], $titulo);

        if(substr($titulo[1][0],0,4) == "(3D)") {
            $ddd = 1;
        } else {
            $ddd = 0;
        }
        $titulo = str_replace("(HD) ","",str_replace("(3D) ","",$titulo[1][0]));
        $titulo = ucwords(str_replace("Á","á",str_replace("É","é",str_replace("Í","í",str_replace("Ó","ó",str_replace("Ú","ú",str_replace("Ñ","ñ",strtolower(utf8_encode($titulo)))))))));
        if(strpos($titulo,'vose') !== false || strpos($titulo,'v.o.s.e.') !== false) {
            $vo = 1;
            $titulo = trim(str_replace("(v.o.s.e.)","",str_replace("(vose)","",trim($titulo))));
        } else {
            $vo = 0;
        }
        $dias[2] = array_slice($dias[2],2);
        $i = 0;
        foreach($dias[2] as $dia) {
            $fecha = explode(" ",strip_tags($dia));
            $fech = $fecha[5]."-".$mesesInv[strtolower($fecha[3])]."-".$fecha[1];

            preg_match_all('/href=\"(.*?)ses=(.*?)\"(.*?)<b>SALA (.*?)<\/b>(.*?)sesion\">(.*?)<\/span>/s',$horas[1][$i],$hora);
            for($a = 0; $a < count($hora[4]); $a++) {
                $cines[$titulo]["Gran Vía Cines"][strtotime($fech)][] = array("sala" => $hora[4][$a], "hora" => $hora[6][$a], "url" => "http://granviacines.reservaentradas.com/cgi-vel/CINES/ww-sel-loc.pro?w-ses=".$hora[2][$a], "3D" => $ddd, "vo" => $vo);
            }
            $i++;
        }
    } else {
        echo $url."<br>";
    }
    echo $count." de ".$tot." - ".$titulo."<br>";
    @ob_end_flush();
    @ob_flush();
    @flush();
    @ob_start();
    sleep($sleep);
}

//Yelmo Cines
echo "<h1>Yelmo Cines</h1>";
$url = "yelmo.html";
$output = file_get_contents($url,false,$context);

preg_match_all('/carteleracine307\">(.*?)<\/tbody>/s', $output, $pelis);
preg_match_all('/<tr (.*?)<\/tr>/s', $pelis[0][0], $pelis);

$count = 0;
$tot = count($pelis[1]);
foreach($pelis[1] as $peli) {
    $count++;
    preg_match_all('/fch(.*?) idf/s', $peli, $f);
    preg_match_all('/181\"><a title=(.*?)\">(.*?)<\/a>/s', $peli, $t);
    preg_match_all('/55\">(.*?)<\/td>/s', $peli, $s);
    preg_match_all('/return false;\">(.*?)<\/a>/s', $peli, $h);
    $fecha = substr_replace(substr_replace($f[1][0], "-", 6, 0), "-", 4, 0);
    $titulo = ucwords($t[2][0]);
    $sala = explode(" ",$s[1][0]);
    $sala = $sala[1];
    $horas = $h[1];
    if(strpos($titulo," 3d") !== false || strpos($titulo,"3d ") !== false || strpos($titulo,"(3d)") !== false) {
        $ddd = 1;
    } else {
        $ddd = 0;
    }
    if(strpos($titulo,'vose') !== false || strpos($titulo,'v.o.s.e.') !== false) {
        $vo = 1;
    } else {
        $vo = 0;
    }
    $titulo = trim(str_replace(" (digital)","",str_replace(" 3d","",str_replace("3d ","",str_replace("(3d)","",$titulo)))));
    $titulo = ucwords(str_replace("Á","á",str_replace("É","é",str_replace("Í","í",str_replace("Ó","ó",str_replace("Ú","ú",str_replace("Ñ","ñ",strtolower(utf8_encode($titulo)))))))));

    foreach($horas as $hora) {
        $cines[$titulo]["Yelmo Cines"][strtotime($fecha)][] = array("sala" => $sala, "hora" => $hora, "url" => "http://www.entradas.com", "3D" => $ddd, "vo" => $vo);
    }
    echo $count." de ".$tot." - ".$titulo." ".$fecha." ".$hora."<br>";
    @ob_end_flush();
    @ob_flush();
    @flush();
    @ob_start();
}

$json_total = array();
$json_total = $cines;

$fp = fopen('../temp/results.json', 'w');
fwrite($fp, json_encode($json_total));
fclose($fp);

?>
<h1>Terminado!</h1>
<p><a href="jsonToDb.php" target="_blank">Ir al procesado de la información</a></p>
