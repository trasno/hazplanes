<?php
/*
 * Scraper de Plaza Eliptica Cines
 * Modificado el 23-01-2016
 */

include_once("../includes/api.inc.php");
include_once("../includes/scrapers.inc.php");

$opts = array('http'=>array('header' => "User-Agent:MyAgent/1.0\r\n"));
$context = stream_context_create($opts);

date_default_timezone_set("Europe/Madrid");

$sleep = "2";
$local = file_get_contents("../temp/results.json");
$json_total = json_decode($local, true);
$mesesInv = array("enero" => "01",
        "febrero" => "02",
        "marzo" => "03",
        "abril" => "04",
        "mayo" => "05",
        "junio" => "06",
        "julio" => "07",
        "agosto" => "08",
        "septiembre" => "09",
        "octubre" => "10",
        "noviembre" => "11",
        "diciembre" => "12");

//Plaza Eliptica
echo "<h1>Plaza eliptica</h1>";
$url = "https://www.reservaentradas.com/cine/pontevedra/cinesplazaeliptica/";
$output = file_get_contents($url,false,$context);

preg_match_all('/title\-movie\-list.*? href="https\:\/\/www\.reservaentradas\.com\/sesiones\/pontevedra\/cinesplazaeliptica\/(.*?)" title/s', $output, $idpelis);

if(!empty($json_total)) {
    foreach($json_total as $k => $json_tipo) {
        foreach($json_tipo as $k2 => $json_evento) {
            if(isset($json_evento["Plaza Elíptica"])) {
                unset($json_total[$k][$k2]["Plaza Elíptica"]);
            }
        }
    }
}

$idpelis = array_unique($idpelis[1]);
$count = array(0,0);
foreach($idpelis as $idpeli) {
    $count[0]++;
    $url = "https://www.reservaentradas.com/sesiones/pontevedra/cinesplazaeliptica/".$idpeli;
    $texto = file_get_contents($url,false,$context);

	preg_match_all('/member\-descriptionX">.*?<h2 .*?><strong>(.*?)<\/strong>/s', $texto, $titulo);

	$titulo = $titulo[1][0];
    $ddd = is3d($titulo);
    $vo = isVo($titulo);
    $titulo = normalize_title($titulo);
    $tipo = getEventType($titulo);

    preg_match_all('/" class="row-fluid">(.*?)<div style=/s', $texto, $sesiones);
	if(count($sesiones[0]) > 0) {
		foreach($sesiones[0] as $sesion) {
        	preg_match_all('/class="hidden-xs"><strong>(.*?)<\/strong><\/div>/s', $sesion, $dia);
			$fechaParts = explode(" ",strip_tags($dia[1][0]));
            $fecha = $fechaParts[5]."-".$mesesInv[strtolower($fechaParts[3])]."-".$fechaParts[1];

			preg_match_all('/sesion hidden-xs .*? href="(.*?)".*?>(.*?)<\/a>/s', $sesion, $horas);
			for($i = 0; $i < count($horas[1]); $i++) {
				$count[1]++;
				if($horas[2][$i] == "00:00") {
                    $horas[2][$i] = "23:59";
                }
                $json_total[$tipo][trim($titulo)]["Plaza Elíptica"][strtotime($fecha)][] = array(
                        "sala" => 1,
                        "hora" => trim($horas[2][$i]),
                        "url" => trim($horas[1][$i]),
                        "3D" => $ddd,
                        "vo" => $vo
                    );
			}
		}
    }
	//sleep($sleep);
}

foreach($json_total as $k => $json_tipo) {
    foreach($json_tipo as $k2 => $json_evento) {
        if(empty($json_evento)) {
            unset($json_total[$k][$k2]);
        }
    }
}

$json_total["plazae"]["last_update"] = date("Y-m-d H:i", strtotime("now"));
$json_total["plazae"]["events"] = $count[0];
$json_total["plazae"]["sessions"] = $count[1];

$fp = fopen('../temp/results.json', 'w');
fwrite($fp, json_encode($json_total));
fclose($fp);

echo "Encontrados ".$count[0]." eventos. Añadidas ".$count[1]." sesiones.";
?>
<h1>Terminado!</h1>